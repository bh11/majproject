package hu.braininghub.fare.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/fare")
public class FareRestController {

    @RequestMapping(value="/hello", method = RequestMethod.GET)
    public String hello() {
        return "hello World from Fare Service";
    }




    }
