/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package store;

import hero.AbstractHero;
import hero.AbstractHero;
import main.InputFromUser;
import hero.MakeShipFunctionalInterface;
import hero.MakeShipFunctionalInterface;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author levsz
 */
public class ShipForHero implements MakeShipFunctionalInterface {
    
    private final int MAX_SIZE_OF_SHIP = 4;
    private String shipName;
    private int id;
    private static int count = 0;
    private List<AbstractHero> heroesOnShip;
    private int heroesNumberOnShip = 0;
    
    public ShipForHero(String n) {
        this.shipName = n;
        heroesOnShip = new ArrayList<>();
        this.id = count++;
    }
    
    
   // List<AbstractHero> ship = new ArrayList<>();
    public boolean isEnoghPlaceForTheNextHero() {
        return heroesNumberOnShip < MAX_SIZE_OF_SHIP;
    }
    
    public void addHero(AbstractHero absh) {
        if (isEnoghPlaceForTheNextHero()){
        heroesOnShip.add(absh);
        heroesNumberOnShip = heroesOnShip.size();
        } else {
            System.out.println("Max place limit is 4!");            
        }
    }
    
    @Override
    public void makeNewShip() {
        new ShipForHero(getNewShipWithUsersName());
    }
    
    private String getNewShipWithUsersName(){
        System.out.println("The "+ this.shipName + " is full. Please name an another, and your new character will be sit in it.");
        InputFromUser in = new InputFromUser();
        return in.getWordFromUser();
    }

    @Override
    public String toString() {
        return "Ship's{" + ", name=" + shipName + ", id=" + id + ", number of heroes: "+ heroesOnShip.size()+'}';
    }

    public int getMAX_SIZE_OF_SHIP() {
        return MAX_SIZE_OF_SHIP;
    }

    public String getShipName() {
        return shipName;
    }

    public int getId() {
        return id;
    }

    public int getCount() {
        return count;
    }

    public List<AbstractHero> getHeroesOnShip() {
        return heroesOnShip;
    }
    
    
    
    
    
}
