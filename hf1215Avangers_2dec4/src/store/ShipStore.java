/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package store;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author levsz
 */
public class ShipStore {
    
    private List<ShipForHero> fleet;
    
    public ShipStore() {
        fleet = new ArrayList<>();
    }

    public List<ShipForHero> getFleet() {
        return fleet;
    }
    
    
    
    public void addShip(ShipForHero ship) {
        fleet.add(ship);
    }
    
        

    @Override
    public String toString() {
        return "Number of ships: "+fleet.size();
    }
    
    
    
}
