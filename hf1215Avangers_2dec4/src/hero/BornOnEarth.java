/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hero;

import Stone.AbstractStone;
import ability.Flying;

/**
 *
 * @author levsz
 */
public class BornOnEarth extends AbstractHero implements Flying {

    private final IdentityCard identifyCard;    
    
    public BornOnEarth(String name, int power, AbstractStone stone, IdentityCard id) {
        super(name, power, stone);
        this.identifyCard = id;
    }

    @Override
    public String toString() {
        return "BornOnEarth{" + "identifyCard=" + identifyCard + "name: "+this.getName()+ '}';
    }

    public IdentityCard getIdentityCard() {
        return identifyCard;
    }
    
    
    
    @Override
    public void fly(){
        System.out.println("I can fly");
    }
    
    
}
