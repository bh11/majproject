/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import Stone.AbstractStone;
import hero.AbstractHero;
import hero.BornOnEarth;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import store.ShipStore;



public class Repo {
    
    Scanner sc = new Scanner(System.in);
    
    
    
//    public AbstractHero initHeroByUser() {
//        System.out.println("please give me a new hero!");
//        
//    }
    
    
    
    public AbstractStone getStoneFromHero(AbstractHero absh) {
        return absh.getStone();
    }
    
    public String getColorFromAbstractStone(AbstractStone absStone) {
        return absStone.getColor();
    }
    
    public int getPowerFromAbstractStone (AbstractStone absStone) {
        return absStone.getPower();
    }
    
    public void printShipsDatas(ShipStore s) {
        s.getFleet().stream().map(ships -> ships.toString()).forEach(System.out::println);
    }
    
    public void printNumberOfHeroes (ShipStore s) {
        long res = s.getFleet().stream().map(shipsforhero -> shipsforhero.getHeroesOnShip()).flatMap(ships -> ships.stream()).map(heroes -> heroes.getName()).count();
        System.out.println(res);
    }
    
    public void printHeroesNames(ShipStore s) {
        s.getFleet().stream().map(x -> x.getHeroesOnShip()).flatMap(c -> c.stream()).map(v -> v.getName()).forEach(System.out::println);
    }
    
    public List<AbstractHero> makeHeroList(ShipStore s) {
        return s.getFleet().stream().map(ships -> ships.getHeroesOnShip()).flatMap(heroes -> heroes.stream()).collect(Collectors.toList());
    }
    
    public void printNumberOfBornOnEarth(ShipStore s) {
        long res = makeHeroList(s).stream().filter(h -> (h instanceof BornOnEarth)).count();
        System.out.println("number of heroes who are from the planet Earth: \n"+res);
    }
    
    public int getHighestPower(ShipStore s) {
        int max = 0;
        List<AbstractHero> list = makeHeroList(s);
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getPower() > max){
                max = list.get(i).getPower();
            }
        }
        return max;
    }
    
    public String getStrongestHero(ShipStore s) {
        int powerPoint = getHighestPower(s);
        List<AbstractHero> list = makeHeroList(s);
        int index = 0;
        while (list.get(index).getPower() != powerPoint){
            index++;
        }
        return list.get(index).getName();
    }
    
    public void listTravelPaperNumbers(ShipStore s) {
        System.out.println("list of travel papers: ");
        List<AbstractHero> list = makeHeroList(s);
        List <AbstractHero> earthHeroList = list.stream().filter(h -> h instanceof BornOnEarth).collect(Collectors.toList());
        for (int i = 0; i < earthHeroList.size(); i++) {
            BornOnEarth member = (BornOnEarth)earthHeroList.get(i);
            System.out.println(member.getIdentityCard().getNumber());
        }
    }
    
    
}
