/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Stone;

/**
 *
 * @author levsz
 */
public abstract class AbstractStone {
    //minden egyes new-nál ott lesz. static - osztály betöltődésekor kapja értékét.
    private static final int MIN_POWER = 2;
    private static final int MAX_POWER = 10;
    
    private final String color; //csak staticnál kell a csupa nagybetű
    private int power; //egy kőnek lesz konkrét színe és neve, létrehzás után nem szeretném változtatni
    
  //  static 
    { //static initialization block
       // MAX_POWER = 10;
        power = 50; //ilyenkor a konstruktor húzódik alá
    }
    
    public AbstractStone (String color){
        this.color = color;
        this.power = generatePower();
    }

    public String getColor() {
        return color;
    }

    public int getPower() {
        return power;
    }

    @Override
    public String toString() {
        return "AbstractStone{" + "color=" + color + ", power=" + power + '}';
    }
    
    private int generatePower() {
        return (int) (Math.random()*(MAX_POWER-MIN_POWER) + MIN_POWER);
        
    }
}
