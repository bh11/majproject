/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animal;

/**
 *
 * @author levsz
 */
public class Fox extends Animal {
    
    private int amountOfTricky; 
    
    public Fox(String name, int age, int tricky){
        super(name,age);
        if (tricky > 0 && tricky < 11){
        amountOfTricky = tricky;
        } else {
            System.out.println("Amount of tricky must be between 0 and 10. This is a wrong answer, tricky is set to -1");
            amountOfTricky = -1;
        }
        this.kind = "Fox";
    }
    
    @Override
    public void say(){
        System.out.println("What the fox say?");
    }
    
    public int getAmountOfTricky(){
        return amountOfTricky;
    }
    
    
}
