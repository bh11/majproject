/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animal;

/**
 *
 * @author levsz
 */
public class Dog extends Animal {
    
    private String color;
    
    
    
    public Dog(String name, int age, String color){
        super(name,age);
        this.countOfLegs = 4;
        this.color = color;
        this.kind = "Dog";
    }
    
    
    @Override
    protected void say(){
        System.out.println("Vau, Vau!");
    }
    
    
}
