/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animal;

/**
 *
 * @author levsz
 */
public class Mainus {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Animal an = new Animal();

        Animal an1 = new Dog("buksi", 3, "black");
        Animal an2 = new Dog("Burkus", 5, "Yellow");
        Animal an3 = new Cat("Lukrécica", 3, false);
        Animal an4 = new Cat("Szerénke", 4, true);
        Fox an5 = new Fox("Ravaszdi", 6, 8); //miért nem érhető el Animal típusú Fox esetén a getTricky?
        Animal an6 = new Fox("buksi", 3, 15);

        an.addNewAnimal(an1);
        an.addNewAnimal(an2);
        an.addNewAnimal(an3);
        an.addNewAnimal(an4);
        an.addNewAnimal(an5);
                an.addNewAnimal(an6);
        
        an.printAnimals();
        System.out.println("");
        System.out.println(an5.getAmountOfTricky());

    }

}
