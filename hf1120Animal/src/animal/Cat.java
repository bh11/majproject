/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animal;

/**
 *
 * @author levsz
 */
public class Cat extends Animal{
    
    private boolean domestic;
    
    
    Cat(String name, int age, Boolean isDomestic){
        super(name,age);
        domestic = isDomestic;
        this.countOfLegs = 4;
        this.kind = "Cat";
    }
    
    
    @Override
    protected void say(){
        System.out.println("Miáu!");
    }
}
