/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animal;

/**
 *
 * @author levsz
 */
public class Animal {
    
    protected String name;
    protected int age;
    protected int countOfLegs;
    private Animal [] members;
    private int counter;
    protected String kind;
    
    public Animal(String name, int age){
        this.name = name;
        this.age = age;
    }
    
    public Animal(){
        members = new Animal[5];
    }
    
    protected void say(){
        System.out.println("I'am an animal");
    }
    
    public void addNewAnimal(Animal an){
        if (counter == members.length){
            increaseNumberOfMembers();
        }
        members[counter] = an;
        counter++;
    }
    private void increaseNumberOfMembers(){
        Animal [] copy = new Animal[counter+1];
        for (int i = 0; i < counter; i++) {
            copy[i] = members[i];
        }
        members = copy;
    }
    
    public void printAnimals(){
        for (int i = 0; i < members.length; i++) {
            System.out.println(i+1+"." + " " + members[i].kind + " "+members[i].name+", age is "+ members[i].age);
            members[i].say();
        }
    }
    
}
