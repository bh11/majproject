/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hf1030;

import java.util.Scanner;

/**
 *
 * @author levsz
 */
public class Harmadik {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int[] array = new int[20];
        int in, in2;
        int i = 0;
        boolean isNumber = false;
        System.out.println("Egész számokat kérek beütni.");

        do {
            if (sc.hasNextInt()) {
                in = sc.nextInt();
                array[i] = in;
                i++;

            } else {
                System.out.println("Számot kérek!");
                sc.next();
            }
        } while (i < 20);

        printArray(array);
        System.out.println("Adjon meg egy számot, melyet megvizsgálink, tartalmazza-e a tömb.");
        do {
            if (sc.hasNextInt()) {
                in2 = sc.nextInt();
                isNumber = true;
                if (indexOfGivenNumberInArray(array, in2) == -1) {
                    System.out.println("Az utoljára megadott szám nem található a tömbben");
                } else {
                    System.out.printf("A megadott szám a %d. helyen található \n", indexOfGivenNumberInArray(array, in2));
                }
            } else {
                System.out.println("Mondomrontombontom számot kérek!");
                sc.next();
            }
        } while (!isNumber);

    }//end of main

    static void printArray(int[] ar) {
        for (int i = 0; i < ar.length; i++) {
            System.out.print(ar[i] + " ");
        }
        System.out.println("");
    }

    static int indexOfGivenNumberInArray(int[] ar, int number) {
        int i = 0;
        while (i<ar.length && ar[i] != number) {
            i++;
        }
        if (i < ar.length) {
            return i+1;
        } else {
            return -1;
        }
    }
}