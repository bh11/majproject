package hf1030;

import static hf1030.Elso.billValue;
import java.util.Scanner;

public class Otodik {

    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);
        System.out.println("Adj be egy szót!");
        String word = sc.next();
        System.out.println("Most egy karaktert!");
        char c = sc.next().charAt(0);
        
        int howMany = numberOfCharacter(word, c);
        
        System.out.printf("A megadott %s szóban a %c karakter %d-szer fordul elő \n",word,c,howMany);
    
    }
    static int numberOfCharacter(String text, char c){
        int count = 0;
        char [] charray = text.toCharArray();
        for (int i = 0; i < charray.length; i++) {
            if (charray[i] == c){
                count++;
            }
        }
        
        return count;
    }
}
