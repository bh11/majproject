/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hf1030;

import java.util.Scanner;

/**
 *
 * @author levsz
 */
public class Negyedik {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int[] ar = new int[10];
        int i = 0;
        boolean isNumber = false;
        while (i < ar.length) {
            ar[i] = (int) (Math.random() * 10);
            i++;
        }
        printArray(ar);
        int sum = sumOfArrayElements(ar);
        double average = averageOfArrayElements(ar);
        System.out.printf("A tömb elemeinek összege: %d . Számtani közepe: %.2f \n", sum, average);

        System.out.println("Most pedig adjon be egy számot 0-9-ig!");

        do {
            if (sc.hasNextInt()) {
                int in = sc.nextInt();
                if (in >= 0 && in <= 9) {
                    //////
                    int howMany = howManyTimesIncludesTheNumber(ar, in);
                    System.out.printf("A megadott szám a tömbben %d-szer fordul elő \n",howMany);
                    isNumber = true;
                } else {
                    System.out.println("A számnak 0-9-ig terjedőnek kell lenni!");
                }
            } else {
                System.out.println("Számot kell megadni 0-9-ig");
                sc.next();
            }
        } while (!isNumber);

    }

    static void printArray(int[] ar) {
        for (int i = 0; i < ar.length; i++) {
            System.out.print(ar[i] + " ");
        }
        System.out.println("");
    }

    static int sumOfArrayElements(int[] ar) {
        int sum = 0;
        for (int i = 0; i < ar.length; i++) {
            sum = sum + ar[i];
        }
        return sum;
    }

    static double averageOfArrayElements(int[] ar) {
        return (double)sumOfArrayElements(ar) / ar.length;
    }
    static int howManyTimesIncludesTheNumber(int [] ar, int number){
        int count = 0;
        for (int i = 0; i < ar.length; i++) {
            if (ar[i] == number){
                count++;
            }
        }
        return count;
    }
}
