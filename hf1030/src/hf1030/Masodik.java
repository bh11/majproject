/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hf1030;


import java.util.Scanner;

/**
 *
 * @author levsz
 */
public class Masodik {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int[] array = new int[100];
        int in;
        int i = 0;
        boolean isZero = false;
        System.out.println("Egész számokat kérek beütni. 0 esetén megállunk");

        do {
            if (sc.hasNextInt()) {
                in = sc.nextInt();
                switch (in) {
                    case 0:
                        isZero = true;
                        break;
                    default:
                        array[i] = in;
                        i++;
                }
            } else {
                System.out.println("Számot kérek!");
                sc.next();
            }
        } while (!isZero);

        reverseOrderArray(array);
        printArray(array);
        minOfElements(array);
        maxOfElements(array);

    }//end of main

    static void reverseOrderArray(int[] ar) {
        for (int i = ar.length - 1; i >= 0; i--) {
            System.out.print(ar[i] + " ");
        }
        System.out.println("");
    }

    static void printArray(int[] ar) {
        for (int i = 0; i < ar.length; i++) {
            System.out.print(ar[i] + " ");
        }
        System.out.println("");
    }

    static void minOfElements(int[] ar) {
        int min = ar[0];
        int place = 0;
        for (int i = 0; i < ar.length; i++) {
            if (ar[i] < min) {
                min = ar[i];
                place = i + 1;
            }
        }
        System.out.printf("A minimum hely a %d. helyen van és értéke %d \n", place, min);
    }

    static void maxOfElements(int[] ar) {
        int max = ar[0];
        int place = 0;
        for (int i = 0; i < ar.length; i++) {
            if (ar[i] > max) {
                max = ar[i];
                place = i + 1;
            }
        }
        System.out.printf("A maximum hely a %d. helyen van és értéke %d \n", place, max);
    }

}
