
package stringpool;



    public class Stringpool {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        String s1 = "Cat";
        String s2 = "Cat";
        String s3 = new String("Cat");
        
        System.out.println("s1 == s2 :"+(s1==s2));
        System.out.println("s1 == s3 :"+(s1==s3));
        System.out.println("s1 == s3.trim? : " + s1 + s3.trim());
        System.out.println("l" + s1.trim() + "l");
        System.out.println("s1.trim().replace('a', 'X').indexOf('t')" + s1.trim().replace('a', 'X').indexOf('t') + "l");
    }
    
}


