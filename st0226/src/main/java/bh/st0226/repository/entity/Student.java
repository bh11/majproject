/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bh.st0226.repository.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author levsz
 */
@Entity
@Table(name = "student") //név megadása azért jó dolog, 
public class Student implements Serializable{
    
    @Id
    @Column(name = "student_id")
    private Integer id;
    
    @Column(name = "student_name")
    private String name;
    
    @Column(name = "neptun_code")
    private String neptunCode;
    
    @Column(name = "date_of_birth")
    private Date dateOfBirth;
    
    @OneToMany(mappedBy = "student", fetch = FetchType.EAGER, cascade = CascadeType.ALL) //azonnal töltődjenek be a hozzá tartozó diákok?
    private List<Subject> subjects;
    //mappedBy = "student". A subjects Subject-eket tároló lista. Azok a Subjectek töltődjenek be, amelyek ehhez a Studenthez tartoznak
    //Ezt érjük el úgy, hogy a Subject-ben a "student" változó fog erre hivatkozni
    
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Student other = (Student) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNeptunCode() {
        return neptunCode;
    }

    public void setNeptunCode(String neptunCode) {
        this.neptunCode = neptunCode;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public List<Subject> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<Subject> subjects) {
        this.subjects = subjects;
    }

    @Override
    public String toString() {
        return "Student{" + "id=" + id + ", name=" + name + ", neptunCode=" + neptunCode + ", dateOfBirth=" + dateOfBirth + ", subjects=" + subjects + '}';
    }
    
    
}
