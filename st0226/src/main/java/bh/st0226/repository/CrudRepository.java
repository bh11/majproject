/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bh.st0226.repository;

import java.util.Optional;

/**
 *
 * @author levsz
 */
public interface CrudRepository<Entity, ID> {
    
    Iterable<Entity> findAll();
    Optional<Entity> findById(ID id);
    void deleteById(ID id);
    void save(Entity entity);
    void update(Entity entity);
    int count();
    
}
