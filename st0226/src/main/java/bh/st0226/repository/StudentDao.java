/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bh.st0226.repository;

import bh.st0226.repository.entity.Student;
import java.util.List;
import java.util.Optional;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author levsz
 */
@Singleton
@LocalBean
public class StudentDao implements CrudRepository<Student, Integer> {

    @PersistenceContext
    private EntityManager em;

//getResultList metódus List-el tér vissza
    //setParameter(mit,mivé) getSingleResult - 
    //2020.03.01.
    @Override
    public Iterable<Student> findAll() {
        return em.createQuery("SELECT e FROM Student e").getResultList();
    }

    public List<Student> findByIdUsingNoOptional(Integer id) {
//ha a úgy kell megadni a neveket (kis-nagy beták), ahogy a db-ben szerepelnek!!!!
        System.out.println("StudentDao findByIdUsingNoOptional - incoming data Integer id is: " + id);//begyün az adat ide!
        List<Student> result = em.createQuery("SELECT s FROM Student s WHERE s.id = :id")
                .setParameter("id", id)
                .getResultList();
        return result;
    }

    @Override
    public Optional<Student> findById(Integer id) {
//ha a úgy kell megadni a neveket, ahogy a db-ben szerepelnek!!!!
        Optional<Student> result = (Optional) em.createQuery("SELECT s FROM Student s WHERE s.id = :id")
                .setParameter("id", id)
                .getSingleResult();
        return result;
    }

    @Override
    public void deleteById(Integer id) {
        //em.remove(Object) ezzel törlünk em.find(class,primary key)
        Student s = em.find(Student.class, id);

        if (s != null) {
            em.remove(s);
        }

    }

    @Override
    public void save(Student entity) {
        em.persist(entity);
    }

    @Override
    public void update(Student entity) {
        em.merge(entity);
    }

    @Override
    public int count() { //diákok száma
        return em.createQuery("SELECT e FROM Student e").getResultList().size();
    }

}
