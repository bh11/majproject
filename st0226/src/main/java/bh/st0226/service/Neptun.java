/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bh.st0226.service;

import bh.st0226.dto.StudentDto;
import bh.st0226.mapper.StudentMapper;
import bh.st0226.repository.StudentDao;
import bh.st0226.repository.entity.Student;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.inject.Inject;

/**
 *
 * @author levsz
 */
@Singleton
//@LocalBean
public class Neptun {
    //Lejönnek az entityk a db-ből a crudRepository interface metódusainak StudentDao megvalósításai által.
    //Nekünk konvertálnunk kell dto objektumokká ezeket, erre hivatott ez a Neptun osztály a mapper osztályok segítségével.
    //Innen a Neptunból már dto-k fognak érkezni a servletekbe. Ezért kell a servletekbe a Neptunt injektálni,
    //a Neptunnak pedig a mappereket, és a StudentDao-t.
    @Inject
    private StudentDao studentDao;
    
    @Inject
    private StudentMapper studentMapper;
    
    
    //kilistázzuk a diákokat
    public List<StudentDto> getStudents(){
        Iterable<Student> list = studentDao.findAll();
        List<StudentDto> resultList = new ArrayList<>();
        //forEach-et tudunk hozni iterable típusra:
        list.forEach(s -> resultList.add(studentMapper.toDto(s)));
        return resultList;        
    }
    
    public StudentDto getStudentById(int id) {
        System.out.println("Neptun-getStudentById- incoming id parameter from GET as int: " + id);
        Integer idNumber = id;
        System.out.println("Neptun-getStudentById- incoming id parameter from GET as Integer: " + id);
        List<Student> student = studentDao.findByIdUsingNoOptional(idNumber);
            if (student.size() == 1) {
                System.out.println("Neptun-getStudentById-studentEntity representation: " + student);
                StudentDto studentDto = studentMapper.toDto((Student)student.get(0));
                System.out.println("In Neptun's getStudentById method - sout -> studentDto" + studentDto);
                return studentDto;
            } else {
             return getDefaultStudentDto();
            }
    }
    
    private StudentDto getDefaultStudentDto() {
        StudentDto defaultStudentDto = new StudentDto();
        defaultStudentDto.setName("Hiba Aladár!");
        defaultStudentDto.setNeptunCode("0");
        return defaultStudentDto;
    }
    
//    public StudentDto getStudentById(int id) {
//        System.out.println("Neptun-getStudentById- incoming id parameter from GET as int: " + id);
//        Integer idNumber = id;
//        System.out.println("Neptun-getStudentById- incoming id parameter from GET as Integer: " + id);
//        Student student = studentDao.findByIdUsingNoOptional(idNumber);
//            if (student != null) {
//                System.out.println("Neptun-getStudentById()-student(Optional) representation: " + student);
//                Student studentEntity = (Student)student.get();
//                System.out.println("Neptun-getStudentById-studentEntity(from Optional to Entity(java obj)) representation: " + studentEntity);
//                StudentDto studentDto = studentMapper.toDto(studentEntity);
//                System.out.println("In Neptun's getStudentById method - sout -> (StudentDto)student.get()" + studentDto);
//                return studentDto;
//            } else {
//             return getDefaultStudentDto();
//            }
//    }
    
}
