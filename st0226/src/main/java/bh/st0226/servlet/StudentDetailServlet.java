/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bh.st0226.servlet;

import bh.st0226.dto.StudentDto;
import bh.st0226.service.Neptun;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author levsz
 */
@WebServlet(name = "StudentDetailServlet", urlPatterns = "/details")
public class StudentDetailServlet extends HttpServlet{
    
    @Inject
    private Neptun neptun;
    
    //@Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        System.out.println("Student's id attribute in StudentDetailServlet: " + id);
        StudentDto student = neptun.getStudentById(id);
        request.setAttribute("resultStudent", student);
        request.getRequestDispatcher("/WEB-INF/studentDetail.jsp").forward(request, response);
    }
    
}
