/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bh.st0226.servlet;

import bh.st0226.dto.StudentDto;
import bh.st0226.service.Neptun;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author levsz
 */
@WebServlet(name = "StudentServlet", urlPatterns = "/students")
public class StudentServlet extends HttpServlet{
    
    public static final String NUMBER_OF_REQUEST = "nrReq";
    public static int numberOfRequestAsInt = 0;
    
    @Inject
    private Neptun neptun;
    
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        List<StudentDto> students = neptun.getStudents();
        request.setAttribute("students", students);
        try {
            System.out.println("in StudentServlet the request attribute is: " + request.getAttribute("students"));
            System.out.println("in StudentServlet the request parameter is: " + request.getParameter("students"));
            System.out.println("getName for attribute: " + students.get(0).getName());
            numberOfRequestAsInt++;
            request.setAttribute("numberOfRequestAsInt", numberOfRequestAsInt);
            System.out.println("numberOfRequestAsInt: " + numberOfRequestAsInt);
            request.getRequestDispatcher("/WEB-INF/students.jsp").forward(request, response);
        } catch (IOException ex) {
            System.out.println("IO exception. There is a problem with the file");
        }
    }
    
    @Override
    public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        System.out.println("number of request " + session.getAttribute(NUMBER_OF_REQUEST));
        if (session.getAttribute(NUMBER_OF_REQUEST) == null) {
            //ilyenkor először jár itt a látogató
            session.setAttribute(NUMBER_OF_REQUEST, 1);
        } else {
            session.setAttribute(NUMBER_OF_REQUEST, (int)session.getAttribute(NUMBER_OF_REQUEST) + 1);
        }
        
        
        super.service(request, response);
    }
    
    private void requestCounterAndSetter(HttpServletRequest request, HttpServletResponse response) {
        String numberOfRequest = (String)request.getAttribute(NUMBER_OF_REQUEST);
        System.out.println(numberOfRequest);
    }
    
         
    
    
    
}
