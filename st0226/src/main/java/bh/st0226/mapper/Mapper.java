/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bh.st0226.mapper;

/**
 *
 * @author levsz
 */
public interface Mapper<Entity, Dto> {
    
    
    Entity toEntity(Dto dto);//dto-t adunk be, és entity fog kijönni belőle
    
    Dto toDto(Entity entity); // entityt adunk be neki és dto fog kijönni
    
    
}
