/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bh.st0226.mapper;

import bh.st0226.dto.StudentDto;
import bh.st0226.dto.SubjectDto;
import bh.st0226.repository.entity.Student;
import bh.st0226.repository.entity.Subject;
import java.util.ArrayList;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.inject.Inject;

/**
 *
 * @author levsz
 */
@Singleton
@LocalBean
public class StudentMapper implements Mapper<Student, StudentDto> {

    //nem tárol állapotot, mivel semmilyen változója sincsen ehhehz
    @Inject
    private SubjectMapper subjectMapper;
    
    
    //Ez az osztály a két metódusának segítségével Student és StudentDto típusok közötti konvergálást végez
    
    @Override
    public Student toEntity(StudentDto dto) { //Tehát beadunk ide egy StudentDto-t. Ezt konvertáljuk át majd Student entityvé 
        Student s = new Student();
        s.setId(dto.getId());
        s.setName(dto.getName());
        s.setDateOfBirth(dto.getDateOfBirth());
        s.setNeptunCode(dto.getNeptunCode());
        s.setSubjects(new ArrayList<>());
        
        for (SubjectDto subjectDto : dto.getSubjects()) {
            s.getSubjects().add(subjectMapper.toEntity(subjectDto));
        }
        
        return s;
    }

    @Override
    public StudentDto toDto(Student entity) {
        StudentDto dto = new StudentDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setNeptunCode(entity.getNeptunCode());
        dto.setDateOfBirth(entity.getDateOfBirth());
        dto.setSubjects(new ArrayList<>());
        
        for (Subject subject : entity.getSubjects()) {
            dto.getSubjects().add(subjectMapper.toDto(subject));
        }
        
        return dto;
    }
    
}
