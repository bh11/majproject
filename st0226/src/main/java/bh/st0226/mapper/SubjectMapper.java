/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bh.st0226.mapper;

import bh.st0226.dto.StudentDto;
import bh.st0226.dto.SubjectDto;
import bh.st0226.repository.entity.Student;
import bh.st0226.repository.entity.Subject;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.inject.Inject;

/**
 *
 * @author levsz
 */
@Singleton
@LocalBean
public class SubjectMapper implements Mapper<Subject, SubjectDto>{

    @Inject
    private StudentMapper studentMapper;
    
    //ez az osztály a mapper fészt megvalósítva, annak két metódus implementációjával végzi a konvertálást
    //SubjectDto és Subject(entityk) között
    /*      *****SubjectDto attribútumai:
     private Integer id;
    private String name;
    private String description;
    private StudentDto student;
    */
    @Override
    public Subject toEntity(SubjectDto dto) {
        Subject s = new Subject();
        s.setId(dto.getId());
        s.setName(dto.getName());
        s.setDescription(dto.getDescription());
        return s;
    }

    @Override
    public SubjectDto toDto(Subject entity) {
        SubjectDto dto = new SubjectDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setDescription(entity.getDescription());
        
        return dto;
    }
    
    //hagyjuk ezt az alábbit...
    private Student getStudentEntityFromSubjectDto(SubjectDto dto) {
        StudentDto studentDto = dto.getStudent();
        Student student = studentMapper.toEntity(studentDto);
        return student;
    }
    
}
