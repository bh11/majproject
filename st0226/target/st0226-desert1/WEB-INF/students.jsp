<%-- 
    Document   : students
    Created on : 2020.03.01., 13:45:37
    Author     : levsz
--%>

<!-- this line below is for avoid the jstl -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <!-- until </head> tag there are some lines for working with bootstrap -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" 
              integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" 
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" 
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" 
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<!-- Ugye azt tudod hapsikám, hogy ez a jsp a WEB-INF-ben van, ezért ez közvetlenül nem hívható meg kliens felől, csak servleten kereztül?-->
    </head>
    <body>
        <h1>Students:</h1>
        <h2>Number of get request of this page (via service method): <c:out value = "${sessionScope.nrReq}"/>      </h2>
        <h2>Number of get request of this page with int solution: <c:out value = "${numberOfRequestAsInt}"/>      </h2>

        <table class="table table-striped table-dark"> >
            <c:forEach var = "s" items = "${students}">
                <tr>
                    <td>
                        <c:out value = "${s.getName()}"/> <!-- getName()-el is működik, csak megcsinálták, 
                                                     hogy ha van gettersetter az attribútumra, akkor le lehet hagyni a get-et -->
                    </td>
                    <td>
                        <a class="btn btn-primary" href="/st0226/details?id=${s.id}" role="button">Informations...</a>
                    </td>
                </tr>
            </c:forEach>
        </table>



    </body>
</html>
