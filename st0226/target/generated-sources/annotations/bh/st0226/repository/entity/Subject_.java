package bh.st0226.repository.entity;

import bh.st0226.repository.entity.Student;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-03-07T21:55:36")
@StaticMetamodel(Subject.class)
public class Subject_ { 

    public static volatile SingularAttribute<Subject, Student> student;
    public static volatile SingularAttribute<Subject, String> name;
    public static volatile SingularAttribute<Subject, String> description;
    public static volatile SingularAttribute<Subject, Integer> id;

}