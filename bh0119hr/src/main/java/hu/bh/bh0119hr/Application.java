/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.bh.bh0119hr;

import java.util.Scanner;

/**
 *
 * @author levsz
 */
public class Application {
    private static final String STOP = "exit";
    
    
    public static void main(String[] args) {
        HrDao dao = new HrDao();
        SwingView view = new SwingView(dao);
        try(Scanner sc = new Scanner(System.in)) {
            
            String line;
            do {
                line = sc.nextLine();
                
                switch (line) {
                    case "1" : dao.findEmployeeWithMaxSalary();
                            break;
                    case "2" : dao.mostCountOfEmployeeOfDepartments(); break;
                    case "3" : dao.hasHigherSalaryThanAverage(); break;
                    case "4" : break;
                    case "5" : break;
                    case "6" : 
                        System.out.println("give me an id unsafe!");
                        String id = sc.nextLine();
                        dao.findEmployeeByIdUnsafe(id); break;
                    case "7" : 
                        System.out.println("give me an id!");
                        String id2 = sc.nextLine();
                        dao.findEmployeeById(id2); break;
                    case "8" : 
                        System.out.println("give me a first name!");
                        String name = sc.nextLine();
                        dao.findEmployeeById(name); break;
                }
                
            } while (!STOP.equalsIgnoreCase(line));
            
        }
        
    }
}
