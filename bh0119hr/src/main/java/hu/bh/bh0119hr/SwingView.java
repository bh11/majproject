/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.bh.bh0119hr;

import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class SwingView extends JFrame {
    
    private HrDao dao;
    private JButton button1 = new JButton("max salary");
    private JButton button2 = new JButton("department with max employee");
    private JButton button3 = new JButton("push");
    private JButton button4 = new JButton("push");
    private JButton button5 = new JButton("push");
    private JTextArea textArea = new JTextArea();
    
    public SwingView(HrDao dao) {
        buildWindow();
        this.dao = dao;
    }
    
    private void buildWindow(){
        setSize(200, 300);
        add(buildNorthPanel(), BorderLayout.NORTH);
        add(buildCentrumPanel(), BorderLayout.CENTER);
        setButtonAction();
        
        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }
    
    private JPanel buildNorthPanel() {
        JPanel north = new JPanel();
        north.add(button1); north.add(button2); north.add(button3); north.add(button4); north.add(button5);
        return north;
    }
    
    private JPanel buildCentrumPanel() {
        JPanel centrum = new JPanel();
        centrum.add(textArea);
        return centrum;
    }
    
    private void setButtonAction() {
        maxSalaryButtonAction();
        maxEmployeeDepartmentAction();
        higherSalaryThanAverageAction();
        
        pack();
    }
    
    private void maxSalaryButtonAction() {
        button1.addActionListener(l -> {
                String result = dao.findEmployeeWithMaxSalary();
                textArea.setText(result);
                        }
        );
    }
    
    private void maxEmployeeDepartmentAction() {
        button2.addActionListener(l -> {
                String result = dao.mostCountOfEmployeeOfDepartments();
                textArea.setText(result);
                        }
        );
    }
    
    private void higherSalaryThanAverageAction() {
        button3.addActionListener(l -> {
                String result = dao.hasHigherSalaryThanAverage();
                textArea.setText(result);
                        }
        );
    }
    
    
}
