/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.bh.bh0119hr;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author levsz
 */
public class HrDao { //igazából nem kellenne sout elem ebben

    private static final String URL = "jdbc:mysql://localhost:3306/hr?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private static final String USER = "root";
    private static final String PW = "mentakivonatok5";

    public String findEmployeeWithMaxSalary() {
        String sqlText = "SELECT * \n"
                + "FROM hr.employees e \n"
                + "WHERE e.salary = (SELECT max(i.salary) FROM hr.employees i);"; //; nem kell itt az utasítás végére, de nem baj ha van
        String res = "";
        try (Connection conn = DriverManager.getConnection(URL, USER, PW);
                Statement stm = conn.createStatement();
                ResultSet rs = stm.executeQuery(sqlText);) {
            
            while (rs.next()) {
                res = rs.getString("first_name") + " " + rs.getString("last_name");
                System.out.println(rs.getString("first_name") + " " + rs.getString("last_name"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    public String mostCountOfEmployeeOfDepartments() {
        String sqlText = "SELECT e.department_id id, count(*) nr FROM hr.employees e GROUP BY e.department_id ORDER BY nr DESC LIMIT 1";
//                + "SELECT d.department_name FROM \n"
//                + "    hr.departments d,\n"
//                + "    (SELECT e.department_id id, count(*) nr \n"
//                + "        FROM hr.employees e \n"
//                + "        GROUP BY e.department_id \n"
//                + "        ORDER BY nr DESC \n"
//                + "        LIMIT 1) i\n"
//                + "WHERE d.department_id = i.id"; //; nem kell itt az utasítás végére, de nem baj ha van
        String result = "";
        try (Connection conn = DriverManager.getConnection(URL, USER, PW);
                Statement stm = conn.createStatement();
                ResultSet rs = stm.executeQuery(sqlText);) {

            while (rs.next()) {
                System.out.println(rs.getString("first_name") + " " + rs.getString("last_name"));
                result += rs.getString("first_name") + " " + rs.getString("last_name") + "\n";
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
    
    public String hasHigherSalaryThanAverage() {
        String sqlText = "SELECT * FROM hr.employees e WHERE e.salary > (SELECT avg(i.salary) FROM hr.employees i)"; //; nem kell itt az utasítás végére, de nem baj ha van
        String result = "";
        try (Connection conn = DriverManager.getConnection(URL, USER, PW);
                Statement stm = conn.createStatement();
                ResultSet rs = stm.executeQuery(sqlText);) {

            while (rs.next()) {
                System.out.println(rs.getString("first_name") + " " + rs.getString("last_name"));
                result += rs.getString("first_name") + " " + rs.getString("last_name") + "\n";
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
//SELECT * FROM hr.employees e WHERE e.hire_date > '1990.01.01'
    
    //SELECT DISTINCT j.job_title title 
//FROM hr.jobs j 
//WHERE j.job_title 
//LIKE '%Clerk%'
//ORDER BY title

    public void findEmployeeByIdUnsafe(String id) {
        String sql =    "SELECT * \n" +
                        "FROM employees e \n" +
                        "WHERE e.employee_id = '" + id + "'";
        
        try (Connection connection = DriverManager.getConnection(URL, USER, PW);
                Statement stm = connection.createStatement()) {
        
        ResultSet rs = stm.executeQuery(sql);
        
        while (rs.next()) {
            System.out.println(rs.getString("first_name") + " " + rs.getString("last_name"));
        }
            
        } catch (SQLException ex) {
            Logger.getLogger(HrDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void findEmployeeById(String id) {
        String sql =    "SELECT * \n" +
                        "FROM employees e \n" +
                        "WHERE e.employee_id = ?";
        
        try (Connection connection = DriverManager.getConnection(URL, USER, PW);
                PreparedStatement stm = connection.prepareStatement(sql)) {
        stm.setInt(1, Integer.parseInt(id));
        ResultSet rs = stm.executeQuery(sql);
        
        while (rs.next()) {
            System.out.println(rs.getString("first_name") + " " + rs.getString("last_name"));
        }
            
        } catch (SQLException ex) {
            Logger.getLogger(HrDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void findEmployeeByFirstName(String name) {
        String sql = "SELECT * \n"
                + "FROM employees e \n"
                + "WHERE e.first_name = ?";
        try (Connection connection = DriverManager.getConnection(URL, USER, PW);
                PreparedStatement stm = connection.prepareStatement(sql)) {
            stm.setString(1, name);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                System.out.println(rs.getString("first_name") + " " + rs.getString("last_name"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(HrDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    
}
