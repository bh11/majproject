<%-- 
    Document   : empnames
    Created on : 2020. jan. 31., 20:52:14
    Author     : czirjak
--%>

<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        
        <table border="1"> 
            
            <%
                List<String> names = (List<String>)request.getAttribute("names");
                
                for (String s : names) {
                    out.print("<tr><td>" + s + "</td></tr>");
                }
            %>
        
        </table>
        
    </body>
</html>

