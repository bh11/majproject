/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.sql.DataSource;
/**
 *
 * @author levsz
 */
@Singleton
public class EmployeeDao {
    
    @Resource(lookup = "jdbc/cz1")
    private DataSource ds;
    
    @PostConstruct
    public void init() {
        System.out.println("It has been initialized.");
    }
    
    @PreDestroy
    public void destroy() {
        System.out.println("It has been destroyed.");
    }
    
    public List<String> getNames() throws SQLException {
        List<String> names = new ArrayList<>();
        
        try (Statement stm = ds.getConnection().createStatement()) {
            String sql = "select * from employees limit 10";
            
            ResultSet rs = stm.executeQuery(sql);
            
            while (rs.next()) {
                names.add(rs.getString("first_name"));
            }
        }
        return names;
    }
    
    public List<String> getSurchedMember(String input) throws SQLException {
        List<String> result = new ArrayList<>();
        
        try (Statement stm = ds.getConnection().createStatement()) {
            String sql = "select * from employees where first_name LIKE '" + input + "%'";
            
            ResultSet rs = stm.executeQuery(sql);
            
            while (rs.next()) {
                 result.add(rs.getString("first_name"));
            }
        }
        return result;
    }
    
}

