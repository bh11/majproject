/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import hu.bh.payaraemployee.service.EmployeeService;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import repository.EmployeeDao;

/**
 *
 * @author levsz
 */
@WebServlet(name = "FindEmployeeServlet", urlPatterns = {"/find"})
public class FindEmployeeServlet extends HttpServlet {

   // EmployeeDao dao;
    @Inject
    private EmployeeDao dao;

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String input = request.getParameter("textForFind");
        System.out.println("textForFind from page: " + input);
        
        
        
        try {
            List<String> list = dao.getSurchedMember(input);
            for (String str : list) {
                System.out.println(str);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(FindEmployeeServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        
    }


}
