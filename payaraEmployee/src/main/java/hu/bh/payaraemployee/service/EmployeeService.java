/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.bh.payaraemployee.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.inject.Inject;
import repository.EmployeeDao;

/**
 *
 * @author levsz
 */
@Stateless
public class EmployeeService {
    
    @Inject
    private EmployeeDao dao;
    
    public List<String> getNames() {
        try {
            return dao.getNames();
            
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeService.class.getName()).log(Level.SEVERE, null, ex);
            
            return new ArrayList<>();
        }
        
    }
    
}

