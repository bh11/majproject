/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bh1105;

import java.util.Scanner;

/**
 *
 * @author levsz
 */
public class HigherAbsInteger {
    static final Scanner SC = new Scanner(System.in);

    public static void main(String[] args) {
        
        int in1 = getNumber();
        int in2 = getNumber();
        System.out.println(higherAbsInteger(in1, in2));
        
    }

    static int getNumber() {
        System.out.println("Give me an integer number!");
        do {
            if (SC.hasNextInt()) {
                int in = SC.nextInt();
                return in;
            } else {
                System.out.println("An integer number what I want!");
                SC.next();
            }
        } while (true);
    }

    static int higherAbsInteger(int a, int b) {
        int aa = Math.abs(a);
        int bb = Math.abs(b);
        if (aa > bb) {
            return aa;
        } else if (bb > aa) {
            return bb;
        } else {
            return 0;
        }
    }
}
