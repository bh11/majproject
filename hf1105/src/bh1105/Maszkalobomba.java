package bh1105;

import java.util.Scanner;

public class Maszkalobomba {

    static final Scanner SC = new Scanner(System.in);
    static final int MAX_SIZE = 15;
    static final int MIN_SIZE = 10;
    static final int MAX_NUMBER_OF_BOMBS = 20;
    static final int MIN_NUMBER_OF_BOMBS = 8;
    static final char PLAYER = 'T';
    static final char GOOD_BOMB = 'B';
    static final char BAD_BOMB = '*';
    static final char EMPTY = '0';
    static final char PRESENT = 'P';

    static int numberOfGoodBombs;
    static int numberOfBadBombs;
    static int row;
    static int column;
    static int lifePoints = 5;
    static int playerX = 0;
    static int playerY = 0;

    static void setZeroField(char[][] field) {
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                field[i][j] = EMPTY;
            }
        }
    }

    static int getNumber(int from, int to) {
        do {
            System.out.printf("Please give me a number between %d and %d \n", from, to);
            if (SC.hasNextInt()) {
                int input = SC.nextInt();
                if (input >= from && input <= to) {
                    return input;
                }
            } else {
                System.out.printf("Your number must be between %d and %d \n", from, to);
            }
        } while (true);
    }

    static int generateNumber(int from, int to) {
        return (int) (Math.random() * (to - from) + from);
    }

    static void setColumnAndRow() {
        System.out.println("Initializing our game field. Give me the height of the columns and rows!");
        row = getNumber(MIN_SIZE, MAX_SIZE);
        column = getNumber(MIN_SIZE, MAX_SIZE);
    }

    static char[][] getField(int column, int row) {
        char[][] field = new char[column][row];
        return field;
    }

    static void printField(char[][] field) {
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                System.out.print(field[i][j] + " ");
            }
            System.out.println("");
        }
    }

    static void setPlayer(char[][] field, char PLAYER) {
        field[0][0] = PLAYER;
    }

    static void numberOfAllBombs(int minNumberOfBombs, int maxNumberOfBombs) {
        System.out.println("Set the number of bombs between 8 and 20!");
        int numberOfBombs = getNumber(minNumberOfBombs, maxNumberOfBombs);
        numberOfGoodBombs = numberOfBombs / 3;
        numberOfBadBombs = numberOfBombs - numberOfGoodBombs;
    }

    static void setBombs(char[][] field, int bombNumber, char bombChar, char empty) {

        for (int i = 0; i < bombNumber; i++) {
            int x = generateNumber(0, column);
            int y = generateNumber(0, row);
            if (field[x][y] == empty) {
                field[x][y] = bombChar;
            } else {
                i--;
            }
        }
    }

    static void putPresent(char[][] field) {
        for (int i = 0; i < 3; i++) {
            int x = generateNumber(0, column);
            int y = generateNumber(0, row);
            if (field[x][y] == EMPTY) {
                field[x][y] = PRESENT;
            } else {
                i--;
            }
        }
    }

    ///////////STEPS
    static void play(char[][] field) {
        while (lifePoints > 0 && !playerAtTheLastRow()) {

            step(field);
        }
    }

    static void step(char[][] field) {
        boolean isChar = false;
        System.out.println("Which movement do you choose? Enter this! r: right, u: up, l: left, d: down");
        do {
            char c = SC.next().charAt(0);
            switch (c) {
                case 'r':
                    stepRight(field);
                    isChar = true;
                    break;
                case 'd':
                    stepDown(field);
                    isChar = true;
                    break;
                case 'l':
                    stepLeft(field);
                    isChar = true;
                    break;
                case 'u':
                    stepUp(field);
                    isChar = true;
                    break;
                default:
                    System.out.println("Enter r, if you want to go right, d for down, l for left and u for up step");
            }
        } while (!isChar);
    }

    static void stepRight(char[][] field) {
        if (!playerAtTheLastColumn()) {
            field[playerX][playerY] = EMPTY;
            playerY++;
            handleBombs(field, playerX, playerY);
            handlePresents(field, playerX, playerY);
            field[playerX][playerY] = PLAYER;
            printField(field);
        }
    }

    static void stepDown(char[][] field) {
        if (!playerAtTheLastRow()) {
            field[playerX][playerY] = EMPTY;
            playerX++;
            handleBombs(field, playerX, playerY);
            handlePresents(field, playerX, playerY);
            field[playerX][playerY] = PLAYER;
            printField(field);
        }
    }

    static void stepLeft(char[][] field) {
        if (!playerAtTheFirstColumn()) {
            field[playerX][playerY] = EMPTY;
            playerY--;
            handleBombs(field, playerX, playerY);
            handlePresents(field, playerX, playerY);
            field[playerX][playerY] = PLAYER;
            printField(field);
        }
    }

    static void stepUp(char[][] field) {
        if (!playerAtTheFirstRow()) {
            field[playerX][playerY] = EMPTY;
            playerX--;
            handleBombs(field, playerX, playerY);
            handlePresents(field, playerX, playerY);
            field[playerX][playerY] = PLAYER;
            printField(field);
        }
    }

    static boolean playerAtTheLastRow() {
        return playerX == row - 1;
    }

    static boolean playerAtTheLastColumn() {
        return playerY == column - 1;
    }

    static boolean playerAtTheFirstColumn() {
        return playerY == 0;
    }

    static boolean playerAtTheFirstRow() {
        return playerX == 0;
    }

    static void handleBombs(char[][] field, int x, int y) {
        if (field[x][y] == GOOD_BOMB) {
            lifePoints--;
        } else if (field[x][y] == BAD_BOMB) {
            lifePoints = 0;
        }
    }

    static void handlePresents(char[][] field, int x, int y) {
        if (field[x][y] == PRESENT) {
            lifePoints++;
        }
    }

///////////////////66
    public static void main(String[] args) {

        //initialize the game
        setColumnAndRow();
        char[][] field = getField(row, column);
        setZeroField(field);
        setPlayer(field, PLAYER);
        numberOfAllBombs(MIN_NUMBER_OF_BOMBS, MAX_NUMBER_OF_BOMBS);
        setBombs(field, numberOfGoodBombs, GOOD_BOMB, EMPTY);
        setBombs(field, numberOfBadBombs, BAD_BOMB, EMPTY);
        putPresent(field);
        printField(field);
        System.out.printf("good: %d bad: %d \n",numberOfGoodBombs, numberOfBadBombs);
        //end of initializing

        play(field);
        if (lifePoints > 0) {
            System.out.printf("You won! Your life: %d \n", lifePoints);
        } else if (lifePoints == 0) {
            System.out.println("You are out of your life points. Game Over!");
        }

    }

}
