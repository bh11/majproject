/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bh1105;

import java.util.Scanner;

/**
 *
 * @author levsz
 */
public class PrintArrayFromEnd {

    public static void main(String[] args) {
        
        int[] ar = {2,4,6,8,10,12,14,16,18,20};
        printArrayFromEnd(ar);
        
    }
    static void printArrayFromEnd(int[] ar){
        for (int i = ar.length-1; i >= 0; i--) {
            System.out.println(ar[i]+" ");
            i--;
        }
    }

}
