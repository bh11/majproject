/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bh1105;

import java.util.Scanner;

/**
 *
 * @author levsz
 */
public class RectangelAndDevider {
    static final Scanner SC = new Scanner(System.in);

    public static void main(String[] args) {

        System.out.print("Side a of rectangle: ");
        int a = getNumber();
        System.out.print("Side b of rectangle: ");
        int b = getNumber();
        System.out.println("");
        System.out.printf("Area of rectangle is %d \n",areaOfRectangle(a, b));
        
        int[] ar = {3,5,15,30,6,9,50,60,21,33};
        System.out.print("3 devider method's return value is: "+countOfDeviderThreeButNotFive(ar)+"\n");
    }

    static int getNumber() {
        do{
            if (SC.hasNextInt()){
                return SC.nextInt();
            } else {
                System.out.println("Not the right form");
                SC.next();
            }
        }while(true);
    }

    static int areaOfRectangle(int a, int b) {
        return a * b;
    }
    static int countOfDeviderThreeButNotFive(int [] ar){
        int count = 0;
        for (int i = 0; i < ar.length; i++) {
            if(ar[i] % 3 == 0 && ar[i] % 5 != 0){
                count++;
            }
        }
        return count;
    }
}
