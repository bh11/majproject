
package bh1105;

import java.util.Scanner;


public class PrimController {
    
    static final Scanner SC = new Scanner(System.in);

    public static void main(String[] args) {

        int[] ar = new int[62];
        int k = 0;
        int countOfDevider = 0;
        for (int i = 1; i < 300; i++) {
            for (int j = 1; j <= i; j++) {
                if (i % j == 0) {
                    countOfDevider++;
                }
            }
            if (countOfDevider == 2) {
                ar[k++] = i;
            }
            countOfDevider = 0;
        }
        for (int i = 0; i < ar.length; i++) {
            System.out.print(ar[i] + " ");
        }
        System.out.println("");
        System.out.println(isPrime(ar));

    }

    static int getNumber() {
        System.out.println("Give me a positive integer number between 1 and 300!");
        do {
            if (SC.hasNextInt()) {
                int in = SC.nextInt();
                if (in <= 300 && in >= 1) {
                    return in;
                } else {
                    System.out.printf("%d is out of 1-300 intervall \n",in);
                }
            } else {
                System.out.println("Positive integer what I want!");
                SC.next();
            }
        } while (true);
    }

    static boolean isPrime(int[] array) {
        int i = 0;
        int in = getNumber();
        while (in >= array[i]) {
            if (array[i] == in) {
                System.out.printf("The given number is the %d. prime number \n", i + 1);
                return true;
            }
            i++;
        }
        return false;
    }
}
