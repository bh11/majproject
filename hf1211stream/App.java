/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package city;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class App {

    static List<Country> countries = new ArrayList<>();

    public static void main(String[] args) {
        generateData();
        System.out.println("print all countries: ");
        printAllCountries();
        printDistinctCountries();
        printCountriesWhereNameContains("Hu");
        printCountOfCountriesWhereNameContains("hu");
        printCountryByCode("sp");
        sortAllOfCountry();
        printContinents();
        printAllCities();
        System.out.println("Hf*******************");
        printSortedCountriesByContinentDesc();
        printEuropeanCities();
    }

    public static void generateData() {
        Country hungary = new Country("hu", "Hungary", "Europe", 93);
        hungary.addCity(new City("Budapest", 2000));
        hungary.addCity(new City("Szeged", 800));
        hungary.addCity(new City("Nyíregyháza", 200));
        hungary.addCity(new City("Tokaj", 30));

        Country spain = new Country("sp", "Span", "Europe", 110);
        spain.addCity(new City("Barcelona", 300));
        spain.addCity(new City("Madrid", 250));

        Country thailand = new Country("th", "Thailand", "Asia", 200);
        thailand.addCity(new City("Phuket", 50));
        thailand.addCity(new City("Bangkok", 3000));

        countries.add(hungary);
        countries.add(hungary);
        countries.add(spain);
        countries.add(thailand);
    }

    //basic
    public static void printAllCountries() {
//        for (Country c : countries) {
//            System.out.print(c.getName()+" ");
//        }
//        countries.stream().forEach(s -> System.out.println(s));
        countries.stream().forEach(System.out::println);
        System.out.println("******************************************");

    }

    //basic
    public static void printDistinctCountries() {
        countries.stream().distinct().forEach(s -> System.out.println(s));
        System.out.println("************************************************");
    }
    

    //basic
    public static void printCountriesWhereNameContains(String str) {
        countries.stream().filter(x -> x.getName().contains(str)).forEach(c -> System.out.println(c));
    }

    //basic
    public static void printCountOfCountriesWhereNameContains(String str) {
        System.out.println(countries.stream().filter((x) -> x.getName().contains(str)).count());
    }

    //basic
    public static void printCountryByCode(String code) {
        countries.stream().filter(x -> x.getCode().equals(code)).forEach(p -> System.out.println("by code: " + p.getCode() + " " + p.getName()));
    }

    //basic
    public static void sortAllOfCountry() {
        System.out.println("sort by alphabetacally");
        countries
                .stream()
                .sorted((x, y) -> x.getName().compareTo(y.getName()))
                .forEach(c -> System.out.println(c.getName()));
    }

    //extra
    public static void printAllCities() {
        System.out.println("print all cities: ");
        countries
                .stream()
                .forEach(c
                        -> c.getCities()
                        .stream()
                        .forEach(cities -> System.out.print(cities + " ")));
        System.out.println("****************************************************************");
    }

    //basic
    public static void printContinents() {
        System.out.println("printContinents:");
        //countries.stream().forEach(x -> System.out.println(x.getName()+"'s continent is "+x.getContinent()));
        countries.stream().map(c -> c.getContinent()).distinct().forEach(x -> System.out.println(x));//map-os rész után már csak a continenteket tartalmazza a lista, erre már lehet distinctelni
        //kiszedem a continenseket, és berakom egy listába:
    }

    //basic
    public static void printCountryNames() {
        countries.stream().forEach(c -> System.out.print(c.getName() + " "));
        System.out.println("");
        System.out.println("*************************************************");
    }

    //basic
    public static void printCountryCodes() {
        System.out.println("print country codes: ");
        countries.stream().forEach(c -> System.out.print(c.getName() + "'s code: " + c.getCode()));
        System.out.println("**********************************************-*****négy");
    }

    //extra
    public static void printSummOffPopulation() {
        int sumOfPop = 0;
        //countries.stream().forEach(c
          //      -> c.getCities().stream());
        countries.stream()
                .mapToInt(
                        country -> country.getCities()
                                .stream()
                                .mapToInt((c) -> c.getPopulation())
                                .sum())
                .sum();
        //countries.get(0).getCities().stream().mapToInt(c -> c.getPopulation()).sum();

    }

    //extra
    public static void printSummOfEuropePopulation() {
        //countries.stream().filter(c -> c.getContinent().equals("Europe"))
              //  .mapToInt(d -> d.getCities())
                
    }

    //extra
    public static void printAllOfCountriesPopulationLessThanX(int x) {

    }
     //HF
    //kontinens neve alapján legyen abc sorrendben csökkenő
    public static void printSortedCountriesByContinentDesc() {
        countries.stream().sorted((c1, c2) -> c1.getContinent().compareTo(c2.getContinent())).forEach(System.out::println);
    }
    
    //városok darabszáma alapján legyen sorrendben
    public static void printSortedCountriesByCountOfCities() {
//        countries.stream().sorted((c1, c2) -> 
//                (c1.getCities().stream().mapToInt(x -> x.getPopulation()).sum())
//                        .compareTo(c2.getCities().stream().mapToInt(z -> z.getPupulation()).sum()))
//                        .forEach(System.out::println);
countries.stream().map(x -> x.getCities()).flatMap(cities -> cities.stream())
        .sorted((c1, c2) -> c1.getPopulation()
                .compareTo(c2.getPopulation()))
        .forEach(System.out::println);
        
                
    }

    
    //kap egy név részletét a városnak, írja ki azokat az országokat ahol ez megtalálható, ne legyen casesensitive
    public static void printCountryWhichContainsPartOfCityName(String partOfCityName) {
        countries.stream().filter(x -> x.getCities().contains(partOfCityName)).forEach(System.out::println);
    }

    //írja ki az országokhoz tartozó méreteket
    public static void printCountrySizes() {
        countries.stream().forEach(x -> System.out.println(x.getName()+"'s size: "+x.getSize()));
    }
    
    //európai városok kiírása
    public static void printEuropeanCities() {
        countries.stream().filter(c -> "Europe".equals(c.getContinent()))
                .map(c -> c.getCities())
                .flatMap(cities -> cities.stream())
                .forEach(System.out::println);
    }

}
