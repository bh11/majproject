/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compare;

/**
 *
 * @author levsz
 */
public class AppStone {
    
    
    
    
    public static void main(String[] args) {
        
        
        StoneCollection firstStoneList = new StoneCollection();
        firstStoneList.addStone(new Stone("vörös homokkő", "üledékes", 400));
        firstStoneList.addStone(new Stone("mészkő", "üledékes", 60));
        firstStoneList.addStone(new Stone("andezit", "vulkáni", 60));
        firstStoneList.addStone(new Stone("kvarcit", "metamorf", 30));
        
        firstStoneList.printStonesFromList();
        firstStoneList.printListByName();
        firstStoneList.printStonesByAgeFromOldest();
        firstStoneList.printListByAgeByMillionYears();
        firstStoneList.printStonesFromList();
        firstStoneList.printStonesFromList2();
        
    }
}
