/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compare;

/**
 *
 * @author levsz
 */
public class Stone {
    
    private int ageByMillionYears;
    private String kindByGenesis;
    private String name;
    private static int idCount = 0;
    private int id;
    
    public Stone(String name, String kind, int weight) {
        this.name = name;
        this.kindByGenesis = kind;
        this.ageByMillionYears = weight;
        this.id = idCount++;
    }
    
    @Override
    public String toString() {
        return "id-"+ this.id +" "+this.name +"-> "+this.kindByGenesis+" and it's age is about "+this.ageByMillionYears+" millions years.";
    }

    
    
    
    
    
    
    
    
    public int getAgeByMillionYears() {
        return ageByMillionYears;
    }

    public void getAgeByMillionYears(int weight) {
        this.ageByMillionYears = weight;
    }

    public String getKindByGenesis() {
        return kindByGenesis;
    }

    public void setKindByGenesis(String kindByGenesis) {
        this.kindByGenesis = kindByGenesis;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static int getIdCount() {
        return idCount;
    }

    public static void setIdCount(int idCount) {
        Stone.idCount = idCount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    
    
    
    
}
