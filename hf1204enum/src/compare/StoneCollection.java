/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compare;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author levsz
 */
public class StoneCollection {

    List<Stone> listOfStones = new ArrayList<>();
    public final String VULCANIC = "vulkáni";
    public final String METAMORF = "metamorf";
    public final String SEDIMENT = "üledékes";

    public void addStone(Stone stone) {
        listOfStones.add(stone);
    }

    public List<Stone> getListOfStones() {
        return listOfStones;
    }

    public void printStonesFromList() {
        System.out.println("Number of stones: " + listOfStones.size());
        for (Stone i : listOfStones) {
            System.out.println(i.toString());
        }
        System.out.println(getNumberOfStoneKinds());
    }

    public void printStonesFromList2() {
        listOfStones.stream().forEach(System.out::println);
    }

    public int getNumberOfVULCANIC() {
        int counter = 0;
        for (int i = 0; i < listOfStones.size(); i++) {
            if (listOfStones.get(i).getKindByGenesis().equals(VULCANIC)) {
                counter++;
            }
        }
        return counter;
    }

    public int getNumberOfSEDIMENT() {
        int counter = 0;
        for (int i = 0; i < listOfStones.size(); i++) {
            if (listOfStones.get(i).getKindByGenesis().equals(SEDIMENT)) {
                counter++;
            }
        }
        return counter;
    }

    public int getNumberOfMETAMORF() {
        int counter = 0;
        for (int i = 0; i < listOfStones.size(); i++) {
            if (listOfStones.get(i).getKindByGenesis().equals(METAMORF)) {
                counter++;
            }
        }
        return counter;
    }

    public String getNumberOfStoneKinds() {
        return "vulkáni: " + getNumberOfVULCANIC() + ", üledékes: " + getNumberOfSEDIMENT() + ", metamorf: " + getNumberOfMETAMORF();
    }

    public void printListByAgeByMillionYears() {
        Collections.sort(listOfStones, new SortByAge());
        printStonesFromList();
    }

    public void printListByName() {
        listOfStones.stream().map(x -> x.getName()).sorted().forEach(x -> System.out.println(x));
    }

    public void printStonesByAgeFromOldest() {
        List<Integer> l = listOfStones.stream().map(x -> x.getAgeByMillionYears()).distinct().sorted((s1, s2) -> s2.compareTo(s1)).collect(Collectors.toList());
        List<Stone> stones = new ArrayList<>();
        for (int i = 0; i < l.size(); i++) {
            for (int j = 0; j < listOfStones.size(); j++) {
                if (listOfStones.get(j).getAgeByMillionYears() == l.get(i)) {
                    System.out.println(listOfStones.get(j).getName() + " (" + listOfStones.get(j).getAgeByMillionYears() + " million years)");
                    stones.add(listOfStones.get(j));
                }
            }
        }
    }

}
