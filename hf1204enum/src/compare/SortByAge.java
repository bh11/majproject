/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compare;

import java.util.Comparator;

/**
 *
 * @author levsz
 */
public class SortByAge implements Comparator<Stone> {

    @Override
    public int compare(Stone s1, Stone s2) {
        return s1.getAgeByMillionYears() - s2.getAgeByMillionYears();

    }
    
}
