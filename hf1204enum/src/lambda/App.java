/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lambda;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author levsz
 */
public class App {

    public static void main(String[] args) {

        FunctionalInterfaceA interfaceA = new FunctionalInterfaceA() {
            @Override
            public void m() {
                System.out.println("Szohi van!");
            }
        };
        
        interfaceA.m();
        
        ClassB b1 = new ClassB(5, "egyes");
        ClassB b2 = new ClassB(2, "kettes");
        ClassB b3 = new ClassB(5, "hármas");
        
        List<ClassB> list = new ArrayList<>();
        
        list.add(b1);
        list.add(b2);
        list.add(b3);
        list.add(new ClassB(10, "négyes"));
        
        //print list before sort:
        for (ClassB i : list) {
            System.out.println(i.toString());
        }
        
        //ebben az esetben a Comparator osztályra csak itt van szükségünk, nem kell külön megírnunk
        //ezért deklaráljuk, és felülírjuk a Collections.sort metóduson belül a compare metódusát
        Collections.sort(list, new Comparator<ClassB>(){
            @Override
            public int compare(ClassB cb1, ClassB cb2){
                return cb1.getNumber()-cb2.getNumber();
            }
        });
        System.out.println("print list after sort by numbers");
        for (ClassB i : list) {
            System.out.println(i.toString());
        }
        System.out.println("************************************************  example for lambda");
        
//        InterfaceA lambda = new InterfaceA(){
//            @Override
//            public void m(){
//                System.out.println("háj!");
//            }
//        };
        
        FunctionalInterfaceA lambda = () -> {
            System.out.println("háj!");
        };
        System.out.println("example for FunctionalInterfaceB");
        
        FunctionalInterfaceB sum = (x, g, z) -> {
            return x+g+z;
        };
        int result = sum.sumOfThreeInteger(4, 7, 10);
        System.out.println(sum.sumOfThreeInteger(4, 7, 10));
        ////////////////////////////////////////////////////////////////6
        List<Integer> numbers = new ArrayList<>();
        Collections.addAll(numbers, -50, -2, 3, 4, 5, 11);
        
        numbers.stream().filter(x -> x<0).forEach(x -> System.out.print(" "+x));
        
        List<Integer> negativesOfNumbers = numbers.stream().filter(x -> x<0).collect(Collectors.toList());
        negativesOfNumbers.stream().forEach(x -> System.out.println(x));
            
        numbers.stream().filter(x -> x<0).map(z -> z * z).forEach(x -> System.out.print(" "+x));
        
        
       
    }

}
