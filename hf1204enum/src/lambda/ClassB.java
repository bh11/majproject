/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lambda;

/**
 *
 * @author levsz
 */
public class ClassB implements FunctionalInterfaceA, FunctionalInterfaceB {

    private int number;
    private String name;
    private int id = 100;
    private static int counter = 0;
    
    public ClassB(int numera, String name) {
        this.name = name;
        this.number = numera;
        this.id += counter;
        counter++;
    }

    public int getNumber() {
        return number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    @Override
    public String toString(){
        return "(id - " + this.id + ") "+ this.name + ", number: " + this.number;
    }
    
    
    @Override
    public void m() {
        System.out.println("Szohie van!");
     }

    @Override
    public int sumOfThreeInteger(int x, int y, int z) {
        return x+y+z;
    }
    
}
