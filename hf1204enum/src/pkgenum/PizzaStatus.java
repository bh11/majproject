/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkgenum;

/**
 *
 * @author levsz
 */
public enum PizzaStatus {
    
    
    ORDERED(5) {
        @Override
        public boolean isOrdered() {
            return true;
        }
        
    },
    READY(2) {
        @Override
        public boolean isReady() {
            return true;
        }
    },
    DELIVERED(0) {
        @Override
        public boolean isDelivered() {
            return true;
        }
    },
    MONDAY(0);
    
    private int something;
    private int timeToDelivery;
    private int id;
    public static int staticId = 0;
    
    public void setSomething(int in) {
        this.something = in;
    }
    
    public int getSomething(){
        return something;
    }
    
    public boolean isOrdered() {
        return false;
    }
    public boolean isReady() {
        return false;
    }
    public boolean isDelivered() {
        return false;
    }
    public int getTimeToDelivery() {
        return timeToDelivery;
    }
    
    public int getId() {
        return this.id;
    }
    
    public void setId() {
        this.id = staticId;
        staticId++;
    }
    
    PizzaStatus(int timeToDelivery) { //implicit private!
        this.timeToDelivery = timeToDelivery;
        System.out.println("Ez most a " + this.toString());
        setId();
    }
    
}

