/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reader;

import java.util.Scanner;

/**
 *
 * @author levsz
 */
public class ConsoleReader {
    
    public String read() {
        try (Scanner sc = new Scanner(System.in)) {
            return sc.nextLine();
        }
    }
}
