/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filterHandler;

import java.io.File;

/**
 *
 * @author levsz
 */
public class SmallLetterContainedFilter implements Filter{

    @Override
    public boolean filter(File file) {
        String name = file.getName();
        for (int i = 0; i < name.length(); i++) {
            if (isSmallLetter(name.charAt(i))) {
                return true;
            }
        }
        return false;
    }
    
    private boolean isSmallLetter(char c) {
        return c >= 'a' && c <= 'z';
    }

    @Override
    public boolean test(File file) {
        return true;
    }
    
    @Override
    public String toString() {
        return "Has low letter in name";
    }
    
}
