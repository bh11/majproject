/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filterHandler;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author levsz
 */
public class AgeLimitedFilter implements Filter{

    private static final String USER_HOME = System.getProperty("user.home");
    
    @Override
    public boolean filter(File file) {
        return file.lastModified() / 1000D / 3600D / 24D < 2; //millisecundumból napba
    }

    @Override
    public boolean test(File file) {
            return file.getPath().contains(USER_HOME);
    }
    
    @Override
    public String toString() {
        return "file older than 2 days";
    }
    
}
