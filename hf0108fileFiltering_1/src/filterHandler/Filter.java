/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filterHandler;

import java.io.File;

public interface Filter {

    boolean filter(File file);
    
    boolean test(File file);
    
    public String toString();
}
