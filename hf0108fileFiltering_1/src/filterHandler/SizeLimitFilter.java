/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filterHandler;

import java.io.File;

/**
 *
 * @author levsz
 */
public class SizeLimitFilter implements Filter{

    @Override
    public boolean filter(File file) {
        return convertToMegabyteFromByte(file.length()) < 2D;     //length() byteokban adja vissza a méretet(amit ő longként ad vissza)
    }
    
    private double convertToMegabyteFromByte(long byteValue) {
        return byteValue / 1024D / 1024D;
    }

    @Override
    public boolean test(File file) {
        return true;
    }

    @Override
    public String toString() {
        return "Size is lower than 2 MB";
    }
    
}
