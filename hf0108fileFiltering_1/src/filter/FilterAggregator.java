/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filter;

import filterHandler.Filter;
import filterHandler.SizeLimitFilter;
import filterHandler.SmallLetterContainedFilter;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author levsz
 */
public class FilterAggregator {
    
    private List<Filter> filters = new ArrayList<>();
    
    public FilterAggregator() {
        filters.add(new SizeLimitFilter());
        filters.add(new SmallLetterContainedFilter());
    }
    
    public List<File> filter(List<File> files) {
        return files.stream()
                .filter(this::allFilterMatched) //f -> allFilterMatched(f)
                .collect(Collectors.toList());
    }
    
    private boolean allFilterMatched(File file) {
        for (Filter filter : filters) {
            if (!filter.filter(file)){
                return false;
            }
        }
        return true;
    }
    
    private List<Filter> collectRelevantFilters(File file) {
        return filters.stream()
                .filter(f -> f.test(file))
                .collect(Collectors.toList());
    }
}
