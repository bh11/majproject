/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filefiltering;

import Collector.FileCollector;
import filter.FilterAggregator;
import java.io.File;
import java.util.List;
import reader.ConsoleReader;

/**
 *
 * @author levsz
 */
public class Application {

    private FileCollector fileCollector = new FileCollector();
    private ConsoleReader consoleReader = new ConsoleReader();
    private final FilterAggregator filterAggregator = new FilterAggregator();
    
    public void process() {
        String path = consoleReader.read();
        List<File> files = fileCollector.collec(new File(path));
                List<File> filteredFiles = filterAggregator.filter(files);
                printResult(filteredFiles);
    }
    
    private void printResult(List<File> files) {
        files.forEach(System.out::println);
    }
    
    
    
    
    
    public static void main(String[] args) {
        
        Application app = new Application();
        app.process();
        
    }
    
}
