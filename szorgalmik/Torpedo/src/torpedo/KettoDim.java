package torpedo;

import java.util.Scanner;

public class KettoDim extends Torpedo {

    static final char EMPTY = ' ';  // [97 - 119]
    static final char ROBOT_SHIP = 'R';
    static final char PLAYER_SHIP = 'P';
    static final char SHOOTED_PLACE = '0';

    static final Scanner SC = new Scanner(System.in);
    static int yP;
    static int xP;
    static int yR;
    static int xR;
    static int shipsOfPlayer;
    static int shipsOfRobot;
    static char[][] playerArea;
    static char[][] robotArea;
    static char[][] robotAreaForPlayer;

    ////////
    static void fillFieldWithNulls(char[][] ar) {
        for (int i = 0; i < ar.length; i++) {
            for (int j = 0; j < ar[i].length; j++) {
                ar[i][j] = EMPTY;
            }
        }
    }

    static void printField(char[][] field) {
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                System.out.print(field[i][j] + " ");
            }
            System.out.println("");
        }
    }

    /**
     * The game in case two dimension torpedo
     */
    static void initGame() {
        initAreas();
        chooseTheNumberOfShips();
        putShipsOnField(robotArea, ROBOT_SHIP, shipsOfRobot, false, false);
        System.out.println("Now you have to put your ships.");
        putShipsOnField(playerArea, PLAYER_SHIP, shipsOfPlayer, false, true);
        printField(playerArea);

    }

    static int getNumber(int from, int to) {
        //System.out.printf("Give me the number between %d and %d! \n", from, to);
        do {
            if (SC.hasNextInt()) {
                int in = SC.nextInt();
                if (in >= from && in < to) {
                    return in;
                } else {
                    System.out.printf("The given number %d is out of argument %d - %d \n", in, from, to);
                }
            } else {
                System.out.printf("The given %s is not a number! \n", SC.next());
            }
        } while (true);
    }

    static int generateNumber(int from, int to) {
        return (int) (Math.random() * (to - from) + from);
    }

    static void initAreas() {
        System.out.println("Choose the size of game places!");
        int x = getNumber(5, 20);
        int y = getNumber(5, 20);
        playerArea = new char[x][y];
        robotArea = new char[x][y];
        robotAreaForPlayer = new char[x][y];
        fillFieldWithNulls(robotArea);
        fillFieldWithNulls(playerArea);
        fillFieldWithNulls(robotAreaForPlayer);
    }

    static int chooseTheNumberOfShips() {
        int maxNumberOfShips = playerArea.length * playerArea[0].length / 2 -1;
        System.out.println("With how many ships do you want to play? Choose a number between 1 and " + maxNumberOfShips);
        shipsOfPlayer = getNumber(1, maxNumberOfShips);
        shipsOfRobot = shipsOfPlayer;
        return shipsOfPlayer;
    }

    static void putShipsOnField(char[][] ar, char character, int count, boolean isRepeatable, boolean isPlayer) {
        if (isPlayer == false) {
            for (int i = 0; i < count; i++) {
                int[] cor = generateCoordinates();
                int x = cor[0];
                int y = cor[1];
                if (ar[x][y] == EMPTY) {
                    ar[x][y] = character;
                } else if (isRepeatable && ar[x][y] == character) {
                    ar[x][y] = character;
                } else {
                    i--;
                }
            }
        } else {
            putPlayersShips(character, count);
        }
    }

    static int[] generateCoordinates() {
        int x = generateNumber(0, playerArea.length);
        int y = generateNumber(0, playerArea[0].length);
        int[] res = {x, y};
        return res;
    }

    static void putPlayersShips(char character, int count) {
        for (int i = 0; i < count; i++) {
            int x = getNumber(0, playerArea.length);
            int y = getNumber(0, playerArea[0].length);
            if (playerArea[x][y] == EMPTY) {
                playerArea[x][y] = character;
            } else if (playerArea[x][y] != EMPTY) {
                System.out.printf("On the (%d ; %d) coordinate you have already a ship. Please choose another place for your war machine! \n", x, y);
                i--;
            }
            printField(playerArea);
        }
    }

    static int numberOfRobotShips() {
        int res = 0;
        for (int i = 0; i < robotArea.length; i++) {
            for (int j = 0; j < playerArea[i].length; j++) {
                if (robotArea[i][j] == ROBOT_SHIP) {
                    res++;
                }
            }
        }
        return res;
    }

    static int numberOfPlayerShips() {
        int res = 0;
        for (int i = 0; i < playerArea.length; i++) {
            for (int j = 0; j < playerArea[i].length; j++) {
                if (robotArea[i][j] != PLAYER_SHIP) {
                    res++;
                }
            }
        }
        return res;
    }

    //////// play game methods
    static void play() {
        while (numberOfPlayerShips() > 0 && numberOfRobotShips() > 0){
        playerShoot();
        robotShoot();
            System.out.println("robot pálya:");
            printField(robotArea);
            System.out.println("robotarea for player:");
            printField(robotAreaForPlayer);
            System.out.println("enyém pálya:");
            printField(playerArea);
        
        
        }
        if (numberOfPlayerShips() == 0){
            System.out.println("You have no more ships :( ");
        } else {
            System.out.println("Congratulations! You won!");
        }

    }

    static void playerShoot() {
        System.out.printf("Shoot the robot's ships! Choose the coordinates betwwen %d and %d! \n", 0, robotArea[0].length);
        int x = getNumber(0, robotArea.length);
        int y = getNumber(0, robotArea[0].length);
        handleShoots(robotArea, x, y, ROBOT_SHIP, true);
    }

    static void robotShoot() {
        int[] cor = generateCoordinates();
        int x = cor[0];
        int y = cor[1];
        handleShoots(playerArea, x, y, PLAYER_SHIP, false);

    }

    static void handleShoots(char[][] shootedArea, int x, int y, char ship, boolean isPlayer) {
        if (shootedArea[x][y] == ship) {
            shootedArea[x][y] = SHOOTED_PLACE;
            if (isPlayer) {
                System.out.println("You hit an enemy ship!");
                robotAreaForPlayer[x][y] = ROBOT_SHIP;
            } else {
                System.out.println("Enemy has shooted a ship of yours :( ");
            }
        } else if (shootedArea[x][y] == SHOOTED_PLACE){
            if (!isPlayer){
                robotShoot();
            } else {
                System.out.println("You've already bombed this point!");
            }
        } else if (shootedArea[x][y] == EMPTY){
            shootedArea[x][y] = SHOOTED_PLACE;
            if (isPlayer){
                System.out.printf("On the (%d ; %d) place there is no ship. \n",x,y);
                robotAreaForPlayer[x][y] = SHOOTED_PLACE;
            }
        }
    }

}
