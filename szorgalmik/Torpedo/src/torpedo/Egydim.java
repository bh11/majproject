
package torpedo;

import java.util.ArrayList;
import java.util.Scanner;


public class Egydim extends Torpedo{
    
//Initializing the beginner objects of the class Egydim
    static Scanner sc = new Scanner(System.in);
    //ha kitörlöm alább a staticot, az enBe metódusnál szólna az enyem hivatkozásánál
    //places of the ships
    static ArrayList<Integer> enyem = new ArrayList<>();
    static ArrayList<Integer> robote = new ArrayList<>();
    //places, where the enemy shoots
    static ArrayList<Integer> enpalya = new ArrayList<>();
    static ArrayList<Integer> robotpalya = new ArrayList<>();

    
/** 1 With this method the player will put down his/her ships between the places 0 and 10. There are three places, what the method save in an ArrayList with the name of enyem. */    
public static void enBe(){
    System.out.println("Most 3 hajót kell leraknod 1-től 10-ig terjedő helyen. A kiválasztott hely után nyomj entert!");
    byte i = 0;
    while (i < 3){
        int be2 = sc.nextInt();
        boolean vanbenne = enyem.contains(be2);
        if (vanbenne != true && be2< 11){
            enyem.add(be2);
            i++;
        }
        else if (vanbenne == true)
            System.out.println(be2+" helyen már van hajód, kérlek válassz másikat!");
        else
            System.out.println("A hajóid csak 1-től 10-ig terjedő helyeken elyezheted el!");
    }
    System.out.println("Hajóid helye: "+enyem.get(0)+" "+enyem.get(1)+" "+enyem.get(2));
}

    /** 2 With this method the robot will put down his/her ships between the places 1 and 10. There are three places, what the method save in an ArrayList with the name of robote. */    
public static void robotBe(){

while (robote.size() < 3){
int rbe = Egydim.random();
boolean vanbenne = robote.contains(rbe);
    if (vanbenne != true )
        robote.add(rbe);
    else if (vanbenne == true){}
}
    for (int i = 0; i < robote.size(); i++) {
    System.out.print("A robot "+(i+1)+". hajója: "+robote.get(i)+", ");   
    }
}

/** 3 It's a random number generator, which give you a number with the value 0-10.*/
public static int random(){
    
    int rC1 = (int) (Math.random() * 10);
    
    return rC1;
}
/** 4 you choose a number before this method (be). Then the 'be' value comes into this method. The method will controll, if the robote includes the 'be' or not.   */
public static void myshoot(int be){
    enpalya.add(be);
    int j = 0;
    while (be != robote.get(j) && j < robote.size()-1){
        j++;
    }
        if (be == robote.get(j)){
            System.out.println("Gratulálok! Eltaláltad a robot egyik hajóját, mely a "+robote.get(j)+" helyen volt.");
            robote.remove(j);
        }
        else if (j == robote.size()-1){
            System.out.println("Sajnos most nem találtad el az ellenség hajóját.");
        }
    System.out.println("Eddigi lövéseid: ");
    for (int i = 0; i < enpalya.size(); i++) {
        System.out.print(enpalya.get(i)+" ");
    }
    System.out.println(" ");
}
/** 5 This method generate a random number between 0-10 with the help of 'random' method. Then see it, that the 'enyem' ArrayList contains it or not. 
 And finally save this number into the 'robotpalya', and next time will not use numbers, what are choosen before */
public static void robotshoot(){
    boolean includeit = true;
    int be;
    while (includeit == true){
    be = Egydim.random();
if (robotpalya.contains(be)){
    includeit = true;
}
else if (enyem.contains(be)){  
    robotpalya.add(be);
    int which = enyem.indexOf(be);
    enyem.remove(which);
    System.out.println("Sajnos a gép eltalálta a következő helyen lévő hajódat: "+be);
    includeit = false;
}
else{
    robotpalya.add(be);
    includeit = false;
}  
    }
}//end of robotshoot
    
    
    
    
    
}//end of Torpedo class
