package elsoproject;

import java.util.HashSet;
import java.util.Set;

public class ElsoProject {

    public static void main(String[] args) {

        Set<Integer> set1 = new HashSet<>();
        Set<Integer> set2 = new HashSet<>();

        System.out.println(set1.add(3));
        System.out.println(set1.add(4));
        System.out.println(set1.add(3));
        System.out.println(set1.add(5));
        
        set2.add(2);
        set2.add(3);
        set2.add(4);
        set2.add(5);
        set2.add(6);
        
        System.out.println("elements of set1: ");
        for (Integer i : set1) {
            System.out.println(i);
        }
        
        System.out.println("size: "+set1.size());
        System.out.println("does set contain 3? "+set1.contains(3));
        set1.remove(3);
        System.out.println("size after remove element 3 : "+set1.size());
        System.out.println("does set contain 3? "+set1.contains(3));
        System.out.println(set1.retainAll(set2)); //set1 - set2
        System.out.println("elements of set1 after set1\\set2");
        for (Integer i : set1) {
            System.out.println(i);
        }
    }

}
