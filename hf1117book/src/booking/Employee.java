/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package booking;

/**
 *
 * @author s26841
 */
public class Employee {
    private String name;
    private String id;
    private BookingUnity [] bookingEntity;
    private final int numberOfUnities = 2;

    public Employee(String name, String id, String unity){
        this.name = name;
        this.id = id;
    }
    
    public Employee(BookingUnity bookingUnity){
        bookingEntity = new BookingUnity[numberOfUnities];
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
}
