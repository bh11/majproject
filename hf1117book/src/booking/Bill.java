/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package booking;

/**
 *
 * @author s26841
 */
public class Bill {
    private String id;
    private int amount;
    
    public Bill(String id, int amount){
        setId(id);
        setAmount(amount);
    }
    
    public Bill(String id){
        this(id, 1000);
    }
    
    public int getAmount() {
        return this.amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
