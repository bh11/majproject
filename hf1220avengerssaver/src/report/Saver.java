/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package report;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import store.Fleet;

/**
 *
 * @author czirjak_zoltan
 */
public class Saver {
    
    private Fleet savedFleet;
    
    public void saveFleet(Fleet f) {
        
        try (FileOutputStream file = new FileOutputStream("fleet.ser")) {
            ObjectOutputStream serializedObject = new ObjectOutputStream(file);
            serializedObject.writeObject(f);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            System.out.println("it's not io exception, but something else: " + e);
        } finally {
            System.out.println("This is because finally");
        }
    }
    
    public Fleet getSavedFleet() {
        savedFleet = null;
        
        try (FileInputStream file = new FileInputStream("fleet.ser")){
            ObjectInputStream objIn = new ObjectInputStream(file);
            this.savedFleet = (Fleet) objIn.readObject();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.out.println("(above is the printStackTrace)");
        } catch (IOException e) {
            System.out.println("This is an io exception");
        } finally {
            System.out.println("You have the saved fleet now!");
        }
        return savedFleet;
    }
    
    
}
