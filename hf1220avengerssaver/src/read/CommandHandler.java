/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package read;

import exceptions.InvalidNameAvengersException;
import hero.AbstractHero;
import hero.HeroFactory;
import report.Reporter;
import report.Saver;
import store.Fleet;


/**
 *
 * @author czirjak_zoltan
 */
public class CommandHandler {
    private static final String DELIMITER = ";";
    private static final int MIN_CHARACTERS_OF_NAME = 2;
    
    private final Fleet store = new Fleet();
    private final Reporter reporter = new Reporter(store);
    private final Saver saver = new Saver();
    
    public void process(String line) throws InvalidNameAvengersException {
        String[] parameters = line.split(DELIMITER);
        checkNameRestriction(parameters[0]);
        AbstractHero hero = HeroFactory.create(parameters);
        store.add(hero);
    }
    
    public void print() {
        System.out.println(store.toString());
    }
    
    public void checkNameRestriction(String name) throws InvalidNameAvengersException {
        if (name.length() < MIN_CHARACTERS_OF_NAME) {
            throw new InvalidNameAvengersException("Invalid name: " + name);
        }
    }
    
    public void report() {
        reporter.generateReports();
    }
    
    public void save() {
    
    }
    
}
