package bh1108ra;

import java.util.Scanner;

public class Bh1108ra {

    public static void main(String[] args) {
        System.out.println("First homework: ");
//1. Celsius to Fahrenheit
        System.out.printf("100°C is %.2f °F \n",celsiusToFahrenheit(33));
        System.out.println("Second homework: ");
//2. which elemnt is the most frequently from an array
        int [] ar = new int[20];
        int [] referentAr = new int[10];
        fillArray(ar);
        makeReferentAr(ar, referentAr);
        System.out.println("ar: ");
        printArray(ar);
        System.out.println("referentAr: ");
        printArray(referentAr);
        System.out.println(whichIsTheMostFrequentedMember(referentAr));
        System.out.println("Third homework: ");
//3. summarize the digits of a number
        System.out.println(sumTheDigitsOfNumber(124));
        System.out.println("Fourth homework: ");
//4. írjunk metódust, ami egy n*m-es kétdimenziós tömböt feltölt véletlenszerű elemekkel valamilyen határok között, 
//majd írjunk egy másik metódust, ami egy n*m-es kétdimenziós tömb legkisebb elemével és annak helyével tér vissza. 
//A visszatérés egy tömb legyen, az alábbi formában [sor, oszlop, érték]
        System.out.println("Please give me two number between 5 and 50!");
        int n = getNumberBetweenRange(5, 50);
        int m = getNumberBetweenRange(5, 50);

        int[][] twoDimAr = new int[n][m];
        initTwoDimArrayWithRandomNumbers(twoDimAr);
        printTwoDimArray(twoDimAr);
        int[] result = minElementInTwoDimArray(twoDimAr);
        System.out.println("The following three number shows the minimum number'datas: {row, column, minimum number}");
        printArray(result);

    }//end of main
    static final Scanner SC = new Scanner(System.in);

    static double celsiusToFahrenheit(double celsius) {
        return (double) (celsius * 1.8 + 32);
    }

    static void fillArray(int[] ar) {
        for (int i = 0; i < ar.length; i++) {
            ar[i] = generateNumber(0, 10);
        }
    }

    static int generateNumber(int from, int to) {
        return (int) (Math.random() * (to - from)) + from;
    }

    static void printArray(int[] ar) {
        for (int i = 0; i < ar.length; i++) {
            System.out.print(ar[i] + " ");
        }
        System.out.println("");
    }

    static void makeReferentAr(int[] ar, int[] referentAr) {
        for (int i = 0; i < ar.length; i++) {
            int number = ar[i];
            referentAr[number]++;
        }
    }

    static int whichIsTheMostFrequentedMember(int[] ar) {
        int max = ar[0];
        for (int i = 0; i < ar.length; i++) {
            if (ar[i] > max) {
                max = ar[i];
            }
        }
        int i = 0;
        while (max != ar[i]) {
            i++;
        }
        return i;
    }

    static int sumTheDigitsOfNumber(int in) {

        if (in > 0) {
            int sum = in % 10;
            return sum + sumTheDigitsOfNumber(in / 10);
        }
        return 0;
    }

    //methods for the task 4
    static void fillArray(int[][] ar) {

    }

    static int getNumberBetweenRange(int from, int to) {
        do {
            if (SC.hasNextInt()) {
                int in = SC.nextInt();
                if (in >= from && in <= to) {
                    return in;
                } else {
                    System.out.printf("The given number %d is out of range (%d - %d) \n", in, from, to);
                }
            } else {
                String in = SC.next();
                System.out.printf("WTF!!! %s in not a number between %d and %d \n", in, from, to);
            }
        } while (true);
    }

    static void initTwoDimArrayWithRandomNumbers(int[][] ar) {
        for (int i = 0; i < ar.length; i++) {
            for (int j = 0; j < ar[i].length; j++) {
                int element = generateNumber(0, 51);
                ar[i][j] = element;
            }
        }
    }

    static void printTwoDimArray(int[][] ar) {
        for (int i = 0; i < ar.length; i++) {
            for (int j = 0; j < ar[i].length; j++) {
                System.out.print(ar[i][j] + " ");
            }
            System.out.println("");
        }
    }

    static int[] minElementInTwoDimArray(int[][] ar) {
        int min = ar[0][0];
        for (int i = 0; i < ar.length; i++) {
            for (int j = 0; j < ar[i].length; j++) {
                if (ar[i][j] < min) {
                    min = ar[i][j];
                }
            }
        }
        int i = 0;
        int j = 0;
        while (ar[i][j] != min) {
            if (j == ar[i].length - 1) {
                i++;
                j = 0;
            } else {
                j++;
            }
        }
        int[] result = {i + 1, j + 1, min};
        return result;
    }
}
