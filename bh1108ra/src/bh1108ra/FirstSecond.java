
package bh1108ra;


public class FirstSecond {
    
    public static void main(String[] args) {
//1. Celsius to Fahrenheit
        System.out.printf("100°C is %.2f °F \n",celsiusToFahrenheit(33));
//2. which elemnt is the most frequently from an array
        int [] ar = new int[20];
        int [] referentAr = new int[10];
        fillArray(ar);
        makeReferentAr(ar, referentAr);
        System.out.println("ar: ");
        printArray(ar);
        System.out.println("referentAr: ");
        printArray(referentAr);
        System.out.println(whichIsTheMostFrequentedMember(referentAr));

        
    }
    static double celsiusToFahrenheit(double celsius){
        return (double)(celsius*1.8+32);
    }
    static void fillArray(int [] ar){
        for (int i = 0; i < ar.length; i++) {
            ar[i] = generateNumber(0,10);
        }
    }
    static int generateNumber(int from, int to){
        return (int)(Math.random()*(to-from))+from;
    }
    static void printArray(int [] ar){
        for (int i = 0; i < ar.length; i++) {
            System.out.print(ar[i]+" ");
        }
        System.out.println("");
    }
    static void makeReferentAr(int [] ar,int [] referentAr){
        for (int i = 0; i < ar.length; i++) {
            int number = ar[i];
            referentAr[number]++;
        }
    }
    static int whichIsTheMostFrequentedMember(int [] ar){
        int max = ar[0];
        for (int i = 0; i < ar.length; i++) {
            if (ar[i] > max){
                max = ar[i];
            }
        }
        int i = 0;
        while (max != ar[i]){
            i++;
        }
        return i;
    }

}
