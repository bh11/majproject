/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.ControllerFace;
import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.util.List;
import java.util.stream.Collectors;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import model.Leader;

/**
 *
 * @author levsz
 */
public class SwingView extends JFrame{
    
    ControllerFace controller;
    private String[] initArray = new String[] {""};
    private JComboBox <String> boxForLeaders = new JComboBox<>(initArray);
    private JComboBox <String> boxForLeadersSouth = new JComboBox<>(initArray);
    
    JLabel addLeaderLabel = new JLabel("new leader: ");
    JLabel addWorkerLabel = new JLabel("new worker: ");
    JLabel printPlaceForDatas = new JLabel();
    JTextField typeLeader = new JTextField("Write new leader's name here: ",20);
    JTextField typeWorker = new JTextField("Add new worker to this leader here: ",20);
    JButton addLeaderButton = new JButton("Add leader");
    JButton addWorkerButton = new JButton("Add worker");
    JButton printDatasButton = new JButton("Print workers");
    JButton saveDatasButton = new JButton("Save");    
    
    
    public void updateWindow() {
        setComboBoxMakingArrayFromList();
        buildWindow();
    }
    
    public void setController(ControllerFace cF) {
        this.controller = cF;
    }
    
    public void setComboBoxMakingArrayFromList() {
        List<String> leadersNames = controller.getLeaderList().getTeam().stream()
                .map(leaders -> leaders.getName())
                .collect(Collectors.toList());
        int lastCurrentIndex = leadersNames.size()-1;
            boxForLeaders.addItem(leadersNames.get(lastCurrentIndex));
            boxForLeadersSouth.addItem(leadersNames.get(lastCurrentIndex));
        pack();        
    }
    
    private JPanel buildNorthPanel() {
        JPanel north = new JPanel();
        north.add(addLeaderLabel); north.add(typeLeader); north.add(addLeaderButton);
        return north;
    }
    
    private JPanel buildCentrumPanel() {
        JPanel centrum = new JPanel();
        centrum.add(boxForLeaders); centrum.add(addWorkerLabel); centrum.add(typeWorker); centrum.add(addWorkerButton); centrum.add(printPlaceForDatas);
        //centrum.setLayout(new GridLayout(2,4));
        boxForLeaders.setEditable(true);
        return centrum;
    }
    
    private JPanel buildSouthPanel() {
        JPanel south = new JPanel();
        south.add(boxForLeadersSouth); south.add(printDatasButton);
        boxForLeadersSouth.setEditable(true);
        return south;
    }
    
    private JPanel buildEastPanel() {
        JPanel east = new JPanel();
        east.add(saveDatasButton);
        return east;
    }
    
    public void buildWindow() {
        add(buildNorthPanel(), BorderLayout.NORTH);
        add(buildCentrumPanel(), BorderLayout.CENTER);
        add(buildSouthPanel(), BorderLayout.SOUTH);
        add(buildEastPanel(), BorderLayout.EAST);
        addLeaderButton.addActionListener(leader -> controller.addLeaderToTeam(typeLeader.getText()));
        addWorkerButton.addActionListener(worker -> controller.addWorkerToLeader
        (boxForLeaders.getItemAt(boxForLeaders.getSelectedIndex()), typeWorker.getText()));
        printDatasButton.addActionListener(l -> printPlaceForDatas.setText
        (controller.getWorkersOfChoosenLeader(boxForLeadersSouth.getItemAt(boxForLeadersSouth.getSelectedIndex()))));
        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }
    
    public void addLeaderToComboBoxes(){
        typeLeader.setText("");
        setComboBoxMakingArrayFromList();
    }
    
    public void setWorkerTextFieldNull() {
        typeWorker.setText("");
    }
    
    
}
