/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author levsz
 */
public class Worker extends AbstractPeople{
    
    public Worker(String name) {
        super(name);
    }

    @Override
    public void addWorkerToLeader(Worker w) {
        System.out.println("This is a worker, who cant have any workers");
    }

    @Override
    public String toString() {
        return "{" + this.id + " " + this.name + " " + this.storyNumber + "\n }  ";
    }
    
    
    
}
