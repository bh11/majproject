/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.ControllerFace;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author levsz
 */
public class LeaderList implements Serializable{
    
    private ControllerFace controller;
    private List<Leader> team = new ArrayList<>();
    
    public void addLeaderToTeam(Leader l) {
        team.add(l);
    }
    
    public void addWorkerToLeaderInTeamByName(String nameOfLeader, String nameOfWorker) {
        List<Leader> surchedLeader = team.stream().filter(leaders -> nameOfLeader.equals(leaders.name)).collect(Collectors.toList());
        if (surchedLeader.size() == 1){
        surchedLeader.get(0).addWorkerToLeader(new Worker(nameOfWorker));
        } else {
            System.out.println("There are two leaders with tis name!");
        }
    }

    public List<Leader> getTeam() {
        return team;
    }
    
    public void setController(ControllerFace cF) {
        this.controller = cF;
    }
    
    public String getWorkersOfChoosenLeader(String leadersName) {
        String res = "";
        List<Leader> surchedLeader = team.stream().filter(leaders -> leadersName.equals(leaders.name)).collect(Collectors.toList());
        if (surchedLeader.size() == 1) {
            for (int i = 0; i < surchedLeader.get(0).getWorkers().size(); i++) {
                res = res + surchedLeader.get(0).getWorkers().get(i).toString();
            }
        }
        System.out.println(res);
        return res;
    }
    
    
    
    
    
    
}
