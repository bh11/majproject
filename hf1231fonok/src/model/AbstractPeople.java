/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.ControllerFace;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author levsz
 */
public abstract class AbstractPeople {
    
    protected String name;
    protected String id;
    protected int storyNumber;
    static private int staticId = 0;
    private static List<Integer> idNumbers= new ArrayList<>();
    private ControllerFace controller;
    
    public AbstractPeople(String name) {
        this.name = name;
        this.storyNumber = staticId++;
        setId();
    }
    
    private void setId() {
        do{
            int result = generateNumber();
            if (!isNumberContainedByIdNumbers(result)) {
                this.id = String.valueOf(result);
                idNumbers.add(result);
                break;
            }
        } while (true);
    }
    
    private int generateNumber() {
        return (int) (Math.random()*900+100);
    }
    
    private boolean isNumberContainedByIdNumbers(int number) {
        for (int i = 0; i < idNumbers.size(); i++) {
            if(idNumbers.get(i) == number) {
                return true;
            }
        }
        return false;
    }
    
    public abstract void addWorkerToLeader(Worker w);

    public String getName() {
        return name;
    }
    
    
    
    
}
