/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 *
 * @author levsz
 */
public class Saver implements Serializable {
    
    private LeaderList serializableLeaders;
    
    public Saver(LeaderList l) {
        this.serializableLeaders = l;
    }
    
    public void serialization() {
        
        try(FileOutputStream file = new FileOutputStream("leaders.ser")) {
            ObjectOutputStream out = new ObjectOutputStream(file);
            out.writeObject(this.serializableLeaders);
        } catch (IOException e) {
            e.printStackTrace();
        }
        
    }
    
    public LeaderList deserialization() {
        Object res = new Object();
        try(FileInputStream file = new FileInputStream("leaders.ser")) {
            ObjectInputStream input = new ObjectInputStream(file);
            res = (LeaderList)input.readObject();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException eee) {
            eee.printStackTrace();
        }
        return res;
    }
    
}
