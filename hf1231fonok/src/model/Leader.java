/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author levsz
 */
public class Leader extends AbstractPeople {
    
    private final String BOSS_SIGN = "BOSS";
    private List<Worker> workers = new ArrayList<>();
    
    
    public Leader(String name) {
        super(name);
        updateIdForBoss();
    }
    
    private void updateIdForBoss() {
        this.id = BOSS_SIGN + this.id; 
    }
    
    @Override
    public void addWorkerToLeader(Worker w) {
        workers.add(w);
    }

    public List<Worker> getWorkers() {
        return workers;
    }

    public void printLeaderWithHisOrHerWorkers() {
        System.out.println("Leader: " + this.storyNumber + " " + this.name + " " + this.id);
        for (Worker w : workers) {
            System.out.println(w.storyNumber + " " + w.name + " " + w.id);
        }
    }
    
    
    
    
    
    
    
}
