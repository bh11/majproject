
package controller;

import model.AbstractPeople;
import model.Leader;
import model.LeaderList;
import model.Worker;
import view.SwingView;
import view.View;

public class Controller implements ControllerFace {

    private LeaderList leaderList;
    private SwingView view;
    
    public Controller(LeaderList model, SwingView view) {
        this.leaderList = model;
        this.view = view;
    }
    
    @Override
    public void addWorkerToLeader(String leader, String worker) {
        leaderList.addWorkerToLeaderInTeamByName(leader, worker);
        view.setWorkerTextFieldNull();
    }

    @Override
    public String getWorkersOfChoosenLeader(String leaderName) {
        return leaderList.getWorkersOfChoosenLeader(leaderName);
    }

    @Override
    public LeaderList getLeaderList() {
        return leaderList;
    }
    
    @Override
    public void addLeaderToTeam(String name) {
        leaderList.getTeam().add(new Leader(name));
        view.addLeaderToComboBoxes();
    }
    
    
}
