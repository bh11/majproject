/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.Leader;
import model.LeaderList;
import model.Worker;

/**
 *
 * @author levsz
 */
public interface ControllerFace {
    
    void addWorkerToLeader(String leader, String worker);
    String getWorkersOfChoosenLeader(String leaderName);
    public LeaderList getLeaderList();
    public void addLeaderToTeam(String name);
    
}
