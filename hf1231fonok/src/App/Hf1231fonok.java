/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App;

import controller.Controller;
import controller.ControllerFace;
import model.AbstractPeople;
import model.Leader;
import model.LeaderList;
import model.Saver;
import model.Worker;
import view.SwingView;

/**
 *
 * @author levsz
 */
public class Hf1231fonok {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        
//        AbstractPeople l1 = new Leader("Gézabá");
//        AbstractPeople l2 = new Leader("Sanyabá");
        
        LeaderList model = new LeaderList();
//        model.getTeam().add((Leader)l1);
//        model.getTeam().add((Leader)l2);
        
        SwingView view = new SwingView();
        ControllerFace controller = new Controller(model, view);
        view.setController(controller);
        model.setController(controller);
        Saver saver = new Saver(model);
        
//        AbstractPeople l1 = new Leader("Gézabá");
//        AbstractPeople l2 = new Leader("Sanyabá");
//        Worker w1 = new Worker("Gézuka");
//        Worker w2 = new Worker("Lackóka");
//        Worker w3 = new Worker("Gyusza");
//        Worker w4 = new Worker("Túrós");
//        
//        l1.addWorkerToLeader((Worker) w1);
//        l1.addWorkerToLeader(w2);
//        l2.addWorkerToLeader(w4);
//        l2.addWorkerToLeader(w3);
        
        
        view.buildWindow();
        
    }
    
}
