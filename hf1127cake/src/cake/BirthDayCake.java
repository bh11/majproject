/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cake;

/**
 *
 * @author levsz
 */
public class BirthDayCake extends Cake {

    public BirthDayCake(int calory, String name) {
        super(calory, name);
    }

    public BirthDayCake(int calory, String name, String wish) {
        super(calory, name);
        this.wish = wish;
    }

    @Override
    public void addComposit(String in) {
        if (counter == ingredients.length) {
            makeBiggerArray();
        } else if ( !isThisAlmostInTheIngredients(in) ) {
            ingredients[counter] = in;
            counter++;
        }
    }

    public void makeBiggerArray() {
        String[] copy = new String[counter + 1];
        for (int i = 0; i < ingredients.length; i++) {
            copy[i] = ingredients[i];
        }
        ingredients = copy;
    }

    public boolean isThisAlmostInTheIngredients(String in) {
        for (int i = 0; i < ingredients.length; i++) {
            if (ingredients[i] != null && ingredients[i].equals(in) ) {
                return true;
            }
        }
        return false;
    }

}
