/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cake;

/**
 *
 * @author levsz
 */
public class Confectionary {

    Sweets[] sweets = new Sweets[5];
    private int counter = 0;

    public Confectionary() {
    }

    public void printSweets() {
        for (int i = 0; i < sweets.length; i++) {
            if (sweets[i] != null) {
                System.out.printf((i + 1) + ". %s's calory content is %d. \n", sweets[i].getName(), sweets[i].getContentOfCalory());
                if (sweets[i] instanceof BirthDayCake) {
                    System.out.println("Wow, You have birthday! " + sweets[i].getWish());
                }
                System.out.println("");
            }
        }
    }

    public void addSweets(Sweets s) {
        if (counter == sweets.length) {
            System.out.println("Sorry, we can't receive more sweets");
        } else {
            sweets[counter] = s;
            counter++;
        }
    }

    private boolean isNullInSweets() {
        for (int i = 0; i < sweets.length; i++) {
            if (sweets[i] == null) {
                return true;
            }
        }
        return false;
    }

    public int numberOfSweets() {

        for (int i = 0; i < sweets.length; i++) {
            if (sweets[i] == null) {
                return i;
            }
        }
        return sweets.length;
    }

    public void printIngredients(Sweets s) {
        System.out.printf("Ingredients of %s: \n", s.getName()); //itt elérhető s.name de why?
        for (int i = 0; i < s.ingredients.length; i++) {
            if (s.ingredients[i] != null) {
                System.out.printf("%d.: %s \n", i, s.ingredients[i]);
            }
        }
    }

    public void printAmountOfCaloryOfSweetsWithNumberOfSweets() {
        System.out.printf("The amount of calory of all (%d pcs) cakes is %d cal \n", numberOfSweets(), amountOfCaloryOfSweets());
    }

    public int amountOfCaloryOfSweets() {
        int res = 0;
        for (int i = 0; i < sweets.length; i++) {
            if (sweets[i] != null) {
                res += sweets[i].contentOfCalory;
            }
        }
        return res;
    }

}
