/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cake;

/**
 *
 * @author levsz
 */
public class Appcake {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Confectionary cuki = new Confectionary();

        //make cakes and birthday cakes
        Cake cake1 = new Cake(500, "Tehén lepény");
        Cake cake2 = new Cake(1500, "a Kék Habos csodatorta");

        BirthDayCake bCake1 = new BirthDayCake(2000, "Ágyútorta", "heppi börzdéj szvídi!");
        BirthDayCake bCake2 = new BirthDayCake(5000, "Rémes krémes");
        bCake2.setWish("Most, hogy megkerülted újra a Napot, Isten éltessen!");
        BirthDayCake bCake3 = new BirthDayCake(3000, "Milliomos kisdobos torta", "Egészségére!");

        //add ingredients to cake1 and bCake1
        cake1.addComposit("liszt");
        cake1.addComposit("tojás");
        cake1.addComposit("ezután több összetevő nem lehet");
        cake1.addComposit("több összetevő nem lehet");

        bCake1.addComposit("liszt");
        bCake1.addComposit("tojás");
        bCake1.addComposit("tojás");
        bCake1.addComposit("cukor");
        bCake1.addComposit("puskapor");
        bCake1.addComposit("álgyúgoló");
        bCake1.addComposit("álgyúgoló");
        bCake1.addComposit("álgyúgoló");
        bCake1.addComposit("álgyúgoló");

        //add cakes to confectionary, and print them
        cuki.addSweets(cake1);
        cuki.addSweets(cake2);
        cuki.addSweets(bCake1);
        cuki.addSweets(bCake2);
        cuki.addSweets(bCake2);
        cuki.addSweets(bCake3);

        cuki.printSweets();

        cuki.printAmountOfCaloryOfSweetsWithNumberOfSweets();

        //controll of ingredients with cake1(where can be max 3) and bcake1 (where can be any) ---> printing
        cuki.printIngredients(cake1);
        System.out.println("");
        cuki.printIngredients(bCake1);

    }

}
