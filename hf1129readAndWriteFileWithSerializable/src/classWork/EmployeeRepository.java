/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classWork;

//package serializationexample;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;



public class EmployeeRepository{
    private static final String FILENAME = "employee.ser";
private static final String FILENAME2 = "oneEmployee.ser";    
    
    public static void serializeEmployee(Set emp){
        try (FileOutputStream fs = new FileOutputStream(FILENAME)){
            ObjectOutputStream ou = new ObjectOutputStream(fs);
            ou.writeObject(emp);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(EmployeeRepository.class.getName()).log(Level.SEVERE,null, ex);
        } catch (IOException ex) {
            Logger.getLogger(EmployeeRepository.class.getName()).log(Level.SEVERE,null, ex);
        }
    }
    
    public static LinkedHashSet<Employee> deserializeEmployee(){
        LinkedHashSet<Employee> emp = null;
        try (FileInputStream fs = new FileInputStream(FILENAME)){
            ObjectInputStream oi = new ObjectInputStream(fs);
            emp = (LinkedHashSet<Employee>) oi.readObject();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(EmployeeRepository.class.getName()).log(Level.SEVERE,null, ex);
        } catch (IOException | ClassNotFoundException ex) {
            
            Logger.getLogger(EmployeeRepository.class.getName()).log(Level.SEVERE,null, ex);
        }
        return emp;
    }
    
    public static void serializeEmployee(Employee emp){
        try (FileOutputStream fs = new FileOutputStream(FILENAME2)){
            ObjectOutputStream ou = new ObjectOutputStream(fs);
            ou.writeObject(emp);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(EmployeeRepository.class.getName()).log(Level.SEVERE,null, ex);
        } catch (IOException ex) {
            Logger.getLogger(EmployeeRepository.class.getName()).log(Level.SEVERE,null, ex);
        }
    }
}

