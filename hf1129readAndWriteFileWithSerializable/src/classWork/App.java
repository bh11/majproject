/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classWork;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 *
 * @author levsz
 */
public class App {
    
    static Set <Employee> employees = new LinkedHashSet<>();

    
    public static void main(String[] args) {
        
                
        generateData();
        
        printText("\nÖsszes dolgozó neve kilistázva");
        printAllEmployee();
        printText("\nA választott dolgozó adatai");
        printOneEmployeeByName("emp4");
        printText("\nÚj dolgozó neve és adatai");
        addEmployee("emp11", 52, 23423, "taska utca", "IT");
        printText("\nDepartment szerint");
        printEmployeesByDepartmentName("IT");
        updateEmployeeSalary("emp2", 213153);
        
        printText("\nEmployee before serialization");
        
        for (Employee employee : employees) {
            System.out.println(employee);
        }
        
        serialize();
        
        printText("\nEmployee after serialization");
        
        for (Employee employee : deserialize()) {
            System.out.println(employee);
        }
        
    }//end of main *********************************************************************************
        public static void generateData(){

            employees.add(new Employee("emp1", 27, 1000000, new Department("kis utca", "IT")));
            employees.add(new Employee("emp3", 50, 543443, new Department("Gézák tere", "Szánkó-factory")));
            employees.add(new Employee("emp4", 40, 3513100, new Department("Délibáb körönd", "TeleHócipő")));
            employees.add(new Employee("emp5", 51, 436400, new Department("KalapKabát út", "IT hazatér")));
            employees.add(new Employee("emp6", 22, 343560, new Department("Egér út", "Macskafogó gyár")));
            employees.add(new Employee("emp2", 32, 651515, new Department("Papkeszi utca", "k_Land")));
            
        }
        
        public static void printText(String string){
            System.out.println(string);
        }
        
        public static void serialize(){
            EmployeeRepository.serializeEmployee(employees);
        }
    
        public static LinkedHashSet<Employee> deserialize(){
            LinkedHashSet<Employee> result = EmployeeRepository.deserializeEmployee();
            return result;
        }
        
        public static void serializeAnEmployee(Employee emp) {
            EmployeeRepository.serializeEmployee(emp);
        }
        
        public static void addEmployee(String name, int age, int salary, String address, String department){
            Employee newEmployee = new Employee(name, age, salary, new Department(address, department));
            employees.add(newEmployee);
            System.out.println(newEmployee);
        }
        
        public static void printAllEmployee(){
            employees.stream()
               .forEach(p -> System.out.println(p));
        }
        
        public static void printOneEmployeeByName(String name){
            employees.stream()
               .filter(p -> p.getName().equals(name))
               .forEach(System.out::println);
        }
        
        public static void printEmployeesByDepartmentName(String departmentName){
            employees.stream()
               .filter(p -> p.getDepartment().getName().equals(departmentName))
               .forEach(p -> System.out.printf(departmentName+ " " + p.getName() + "\n"));
        }
        
        public static void updateEmployeeSalary(String name, int salary){ 
            employees.stream()
               .filter(p -> p.getName().equals(name))
               .findAny()
               .ifPresent(p -> {
                   p.setSalary(salary);
               });           
        }
        
        public static void removeEmployeeByName(String name){
            employees.stream().filter(e -> name.equals(e.getName())).skip(0);
        }           
}

