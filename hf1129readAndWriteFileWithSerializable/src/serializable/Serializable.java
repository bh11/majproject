/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serializable;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 *
 * @author levsz
 */
public class Serializable {

    public static void printData(SerializedClassA obj){
        System.out.println("name: "+obj.getName());
        System.out.println("age: "+obj.getAge());
        System.out.println("a: "+obj.getA());
        System.out.println("b: "+obj.b);
    }
    
    
    public static void main(String[] args) {
        
        SerializedClassA obj1 = new SerializedClassA("elso", 29, 5, 10);
        String fileName = "serialization.ser";
        try{
            FileOutputStream file = new FileOutputStream(fileName);
            ObjectOutputStream out = new ObjectOutputStream(file);
            
            out.writeObject(obj1);
            out.close();
            file.close();
            printData(obj1);
            obj1.b = 2000;            
            
        } catch(IOException e) {
            System.out.println("IOException is caught");
        }
        obj1 = null;
        try {
            FileInputStream file = new FileInputStream(fileName);
            ObjectInputStream in = new ObjectInputStream(file);
            Object obj2;
            obj2 = in.readObject();
            System.out.println("after serialization:");
            printData((SerializedClassA) obj2);
            
        } catch (IOException e) {
            System.out.println("This is an IOException");
        } catch (ClassNotFoundException e) {
            System.out.println("Class is not found.");
        }
        
    }
    
}
