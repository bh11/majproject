/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serializable;
import java.io.*;

/**
 *
 * @author levsz
 */
public class SerializedClassA implements java.io.Serializable{
    private static final long serialversionUID = 
                                 129348938L;
    
    transient private int a;
    static int b;
    private String name;
    private int age;
    
    public SerializedClassA(String name, int age, int a, int b) {
        this.a = a;
        this.age = age;
        this.name = name;
        this.b = b;
    }

    public int getA() {
        return a;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }
    
    
    
}
