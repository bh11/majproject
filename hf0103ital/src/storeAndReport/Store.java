/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package storeAndReport;

import drinks.AbstractDrink;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author levsz
 */
public class Store {
    
    private List<AbstractDrink> drinkList = new ArrayList<>();
    
    public void addNewAbstractDrinkToList(AbstractDrink drink) {
        drinkList.add(drink);
    }

    public List<AbstractDrink> getDrinkList() {
        return drinkList;
    }
    
    
    
}
