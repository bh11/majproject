/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package storeAndReport;

/**
 *
 * @author levsz
 */
public class Report {
    
    private Store store;
    
    public Report(Store store) {
        this.store = store;
    }
    
    public void printAllDrink() {
        System.out.println("all drinks: ");
        store.getDrinkList().stream().forEach(drink -> System.out.println(drink.toString()));
        System.out.println("************************************************************");
    }
    
    public void printIndusties() {
        System.out.println("Industries: ");
        store.getDrinkList().stream().map(drink -> drink.getIndustry().getName()).distinct().forEach(System.out::println);
        System.out.println("**********************************************************************************");
    }
    
}
