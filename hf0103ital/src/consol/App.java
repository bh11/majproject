/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package consol;

import drinks.AbstractDrink;
import drinks.Industry;
import enums.Colors;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import storeAndReport.Report;
import storeAndReport.Store;

/**
 *
 * @author levsz
 */
public class App {
    private static final String STOP_SIGN = "stop";
    private ConsoleInput consoleInput;
    
    public App(ConsoleInput consoleInput){
        this.consoleInput = consoleInput;
    }
    
    public void inputLineFromUser() {
        String in = "";
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            do{
                in = br.readLine();
                consoleInput.handleInputString(in);
            }while(!STOP_SIGN.equalsIgnoreCase(in));
            System.out.println("end of input");
        } catch (IOException ex) {
            System.out.println("IO exception"); ex.printStackTrace();
        }
    }

    
    public static void main(String[] args) {
        
        Store store = new Store();
        ConsoleInput handleInput = new ConsoleInput(store);
        App app = new App(handleInput);
        Report report = new Report(store);
        InputFromFile fileInput = new InputFromFile(handleInput, store);
        
        //app.inputLineFromUser();
        fileInput.getDatasFromFile();
        
        
        report.printAllDrink();
        report.printIndusties();
        
       
        
        
        
        
        
//        Store store = new Store();
//        Industry industry1 = new Industry("Torley", "Nagyteteny");
//        Industry industry2 = new Industry("Augustiener", "Munchen");
//        AbstractDrink ad1 = new AbstractDrink("beer", industry1, 200, 5) {};
//        AbstractDrink ad2 = new AbstractDrink("cognac", industry1, 600, 5) {};
//        AbstractDrink ad3 = new AbstractDrink("beer", industry2, 600, 10) {};
//        AbstractDrink ad4 = new AbstractDrink("wien", industry1, 500, 15) {};
//        
//        store.addNewAbstractDrinkToList(ad1); store.addNewAbstractDrinkToList(ad2); store.addNewAbstractDrinkToList(ad3);
//        store.addNewAbstractDrinkToList(ad4);
//        
//        store.getDrinkList().stream().map(absdrink -> absdrink.getDrinkType().toString()).forEach(System.out::println);
        
        
        
    }
    
}
