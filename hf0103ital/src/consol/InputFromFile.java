/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package consol;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import storeAndReport.Store;

/**
 *
 * @author levsz
 */
public class InputFromFile {
    
    private ConsoleInput consoleInput;
    private Store store;

    public InputFromFile(ConsoleInput consoleInput, Store store) {
        this.consoleInput = consoleInput;
        this.store = store;
    }
    
    public void getDatasFromFile() {
        List<String> res = new ArrayList<>();
        File file = new File("c:\\Users\\levsz\\alcohol.txt");
        try(BufferedReader br = new BufferedReader(new FileReader(file))){
            String str = "";
            while((str = br.readLine()) != null){
                res.add(str);
                consoleInput.handleInputString(str);
            }
        } catch (IOException ex) {
            Logger.getLogger(InputFromFile.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
