/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package consol;

import drinks.AbstractDrink;
import drinks.Industry;
import java.util.List;
import storeAndReport.Store;

/**
 *
 * @author levsz
 */
public class ConsoleInput {
    private static final int AMOUNT_OF_ELEMENTS = 5;
    private static final int CONTROL_NUMBER = -2;
    private Store store;
    
    public ConsoleInput(Store store) {
        this.store = store;
    }
    
    public void handleInputString(String input) {
        if ("stop".equalsIgnoreCase(input)){
            return;
        }
        String[] stringArray = splitInputString(input);
        int[] lastTwoNumber = getLastTwoNumberFromInput(stringArray);
        int control = lastTwoNumber[0] + lastTwoNumber[1];
        if(hasCorrectAmountOfElements(stringArray) && control != CONTROL_NUMBER){
            String industryName = stringArray[0];
            String industryAdress = stringArray[1];
            String drinkType = stringArray[2];
            int drinkPrise = lastTwoNumber[0];
            int alcoholVol = lastTwoNumber[1];
            store.addNewAbstractDrinkToList(new AbstractDrink(drinkType, new Industry(industryName, industryAdress), drinkPrise, alcoholVol));
            System.out.println("Adding new drink was succesful");
        } else if (control == CONTROL_NUMBER) {
            System.out.println("Wrong format for number");
        } else {
            System.out.println("Incorrect input");
        }
    }
    
    private String[] splitInputString(String input) {
        String[] ar = input.split(" ");
        return ar;
    }
    
    private boolean hasCorrectAmountOfElements(String [] ar) {
        return ar.length == AMOUNT_OF_ELEMENTS;
    }
    
    private int[] getLastTwoNumberFromInput(String[] ar) {
        
        try{
            int length = ar.length;
            int last = Integer.parseInt(ar[length-1]);
            int beforeLast = Integer.parseInt(ar[length-2]);
            return new int[] {beforeLast,last};
        } catch (NumberFormatException e) {
            e.printStackTrace();
            System.out.println("Wrong format for number");
            return new int[] {-1,-1};
        }
    }
    
}
