/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drinks;

import enums.DrinkType;

/**
 *
 * @author levsz
 */
public class AbstractDrink {

    private DrinkType drinkType;
    private Industry industry;
    private int code;
    private int prise;
    private int alcoholContentInPercent;

    public AbstractDrink(String drinkType, Industry industry, int prise, int alcoholContentInPercent) {
        setDrinkType(drinkType);
        this.industry = industry;
        this.prise = prise;
        this.alcoholContentInPercent = alcoholContentInPercent;
        this.code = generateNumber();
    }

    private int generateNumber() {
        return (int) (Math.random() * 10000 + 90000);
    }

    private void setDrinkType(String nameOfDrinkType) {
        DrinkType[] drinks = DrinkType.values();
        for (DrinkType dt : drinks) {
            if (nameOfDrinkType.equalsIgnoreCase(dt.toString())) {
                this.drinkType = dt;
            }
        }
        if (this.drinkType == null) {
            System.out.println("We have only beer, wine and snaps. This kind of alcohol is setted defaultly beer");
            this.drinkType = DrinkType.BEER;
        }
    }

    public DrinkType getDrinkType() {
        return drinkType;
    }

    public Industry getIndustry() {
        return industry;
    }

    public int getCode() {
        return code;
    }

    public int getPrise() {
        return prise;
    }

    public int getAlcoholContentInPercent() {
        return alcoholContentInPercent;
    }

    @Override
    public String toString() {
        return drinkType + " " + industry + ", code=" + code + ", prise=" + prise + ", alcoholContentInPercent=" + alcoholContentInPercent + '}';
    }
    
    

}
