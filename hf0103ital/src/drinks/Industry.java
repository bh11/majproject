/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drinks;

/**
 *
 * @author levsz
 */
public class Industry {
    
    private String name;
    private String adress;

    public Industry(String name, String adress) {
        this.name = name;
        this.adress = adress;
    }

    
    
    
    public String getName() {
        return name;
    }

    public String getAdress() {
        return adress;
    }

    @Override
    public String toString() {
        return "Industry{" + name + " "+ adress + '}';
    }
    
    
}
